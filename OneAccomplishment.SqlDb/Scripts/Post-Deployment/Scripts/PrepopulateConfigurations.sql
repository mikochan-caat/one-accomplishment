﻿-- =============================================
-- Prepopulates the Configurations table with 
-- the defaults
-- =============================================
PRINT 'Prepopulating Configurations table...'
GO

TRUNCATE TABLE [Configurations]
GO

-- Cast the Value column of the first row to sql_variant 
-- in order to allow multiple data types in the VALUES clause
INSERT INTO [Configurations] (Name, DataType, Value, [Description])
VALUES
	('LoginAttemptMaxLimit', 'int', CAST(4 AS sql_variant), 'Number of extra tries in case of a failed login attempt.'),
	('LoginAttemptExceededTimeout', 'int', 30, 'Timeout in seconds when repeated failed login attempts occur.'),
	('StayLoggedInMaxDuration', 'float', 168, 'Duration in hours for the login session when using the Stay Logged In feature.'),
	('StayLoggedInDefaultDuration', 'float', 1, 'Duration in hours for the normal login session.'),
	('TempDataCorrelationParamName', 'nvarchar(MAX)', 'tdp_c', 'The reserved query string parameter for TempData correlation.'),
	('TempDataCorrelationFormName', 'nvarchar(MAX)', '__tdp_c+', 'The form input name used for TempData correlation.'),
	('TempDataCorrelationCookieName', 'nvarchar(MAX)', 'TDP_Correlator', 'The cookie name used for TempData correlation.')
GO
