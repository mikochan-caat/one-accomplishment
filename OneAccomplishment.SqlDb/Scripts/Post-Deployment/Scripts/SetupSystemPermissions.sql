﻿-- =============================================
-- Setup all default system Roles and Permissions
-- =============================================
PRINT 'Setting up system roles and permissions mapping...'
GO

-- Setup Permissions
-- All permission categories
DECLARE @AdministrationCategory AS varchar(MAX) = 'Administration'

SET IDENTITY_INSERT [Permissions] ON

DECLARE @TempPermissions AS TABLE(PermissionId bigint, 
								  Category varchar(MAX), 
								  PermissionName varchar(MAX),
								  [Description] varchar(MAX))
INSERT INTO @TempPermissions (PermissionId, Category, PermissionName, [Description])
VALUES
	(1, @AdministrationCategory, 'CreateRole', 'Allows creation of new roles.'),
	(2, @AdministrationCategory, 'ReadRole', 'Allows viewing of roles.'),
	(3, @AdministrationCategory, 'UpdateRole', 'Allows modification of non-builtin roles.'),
	(4, @AdministrationCategory, 'DeleteRole', 'Allows deletion of non-builtin roles.'),
	(5, @AdministrationCategory, 'AssignRole', 'Allows assigning of roles to others.'),
	(6, @AdministrationCategory, 'AdministratorClaim', 'Administrator Designation.')

MERGE INTO [Permissions] DST
USING @TempPermissions AS SRC
ON SRC.PermissionId = DST.PermissionId
WHEN MATCHED THEN
	UPDATE SET Category = SRC.Category,
			   PermissionName = SRC.PermissionName,
			   [Description] = SRC.[Description]
WHEN NOT MATCHED THEN 
	INSERT (PermissionId, Category, PermissionName, [Description])
	VALUES (SRC.PermissionId, SRC.Category, SRC.PermissionName, SRC.[Description]);

SET IDENTITY_INSERT [Permissions] OFF
GO

-- Setup Roles
SET IDENTITY_INSERT Roles ON

DECLARE @TempRoles AS TABLE(RoleId bigint, RoleName varchar(MAX), [Description] varchar(MAX))
INSERT INTO @TempRoles (RoleId, RoleName, [Description])
VALUES
	(0, 'Super Administrator', 'Full administrator with super user privileges. Typically for the site owners/developers.'),
	(1, 'Administrator', 'Administrator with a restricted set of privileges. Recommended when giving power to non-site owners.'),
	(2, 'Member', 'Normal site user.'),
	(3, 'Guest', 'Site user that can only perform viewing on a restricted set of data.')
	
MERGE INTO Roles DST
USING (
	SELECT *, 1 AS IsBuiltIn
	FROM @TempRoles
) AS SRC
ON SRC.RoleId = DST.RoleId
WHEN MATCHED THEN
	UPDATE SET RoleName = SRC.RoleName,
			   [Description] = SRC.[Description]
WHEN NOT MATCHED THEN 
	INSERT (RoleId, IsBuiltIn, RoleName, [Description])
	VALUES (SRC.RoleId, SRC.IsBuiltIn, SRC.RoleName, SRC.[Description]);

SET IDENTITY_INSERT Roles OFF
GO

-- Setup Role Mappings
DELETE FROM RoleMappings
WHERE RoleId IN (0, 1, 2, 3)
-- Reset identity value
DBCC CHECKIDENT ('RoleMappings', RESEED, 0)

;WITH cte_RoleMappings(RoleId, PermissionId)
AS
(
	-- Super Administrator
	SELECT 0, * FROM (VALUES (5), (6)) P(Id) UNION ALL
	-- Administrator	
	SELECT 1, * FROM (VALUES (1), (2), (3), (4), (6)) P(Id)
)
INSERT INTO RoleMappings(RoleId, PermissionId)
SELECT *
FROM cte_RoleMappings
GO
