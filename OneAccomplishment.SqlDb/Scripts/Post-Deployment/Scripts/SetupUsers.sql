﻿-- =============================================
-- Create conditionally defined Users
-- =============================================
PRINT 'Setting up database users...'
GO

-- IIS APPPOOL\DefaultAppPool
-- Only create if it is defined in the server-level logins
IF (EXISTS(SELECT 1 FROM [master].[dbo].[syslogins] WHERE LoginName = 'IIS APPPOOL\DefaultAppPool'))
BEGIN
	CREATE USER [IIS APPPOOL\DefaultAppPool] FOR LOGIN [IIS APPPOOL\DefaultAppPool]
END
GO
