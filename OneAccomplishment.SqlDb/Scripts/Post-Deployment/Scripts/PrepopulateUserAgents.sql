﻿-- =============================================
-- Prepopulates the UserAgents table with 
-- the defaults
-- =============================================
PRINT 'Prepopulating UserAgents table...'
GO

SET IDENTITY_INSERT UserAgents ON

MERGE INTO UserAgents DST
USING (
	SELECT 0 AS UserAgentId, '<Unknown User Agent>' AS UAString
) AS SRC
ON SRC.UserAgentId = DST.UserAgentId
WHEN MATCHED THEN
	UPDATE SET UAString = SRC.UAString
WHEN NOT MATCHED THEN 
	INSERT (UserAgentId, UAString)
	VALUES (SRC.UserAgentId, SRC.UAString);

SET IDENTITY_INSERT UserAgents OFF
GO
