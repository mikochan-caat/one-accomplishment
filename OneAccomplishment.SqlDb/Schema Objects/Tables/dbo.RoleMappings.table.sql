﻿/**
 * This table provides the batching of permissions into a single role.
 */
CREATE TABLE [dbo].[RoleMappings]
(
	RoleMappingId bigint NOT NULL IDENTITY(1, 1),
	RoleId bigint NOT NULL,
	PermissionId bigint NOT NULL

	CONSTRAINT PK_RoleMappings
		PRIMARY KEY CLUSTERED (RoleMappingId)
	CONSTRAINT FK_RoleMappings_Roles
		FOREIGN KEY (RoleId) REFERENCES Roles(RoleId)
	CONSTRAINT FK_RoleMappings_Permissions
		FOREIGN KEY (PermissionId) REFERENCES [Permissions](PermissionId)
)
GO

CREATE UNIQUE INDEX UX_RoleMappings_RoleId_PermissionId
	ON RoleMappings(RoleId, PermissionId)
GO
