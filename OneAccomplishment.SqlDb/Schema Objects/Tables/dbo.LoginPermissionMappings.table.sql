﻿/**
 * This table provides fine-grained permission control per user login.
 */
CREATE TABLE [dbo].[LoginPermissionMappings]
(
	LoginRoleMappingId bigint NOT NULL IDENTITY(1, 1),
	LoginId bigint NOT NULL,
	PermissionId bigint NOT NULL,
	IsGranted bit NOT NULL

	CONSTRAINT PK_LoginPermissionMappings
		PRIMARY KEY CLUSTERED (LoginRoleMappingId)
	CONSTRAINT FK_LoginPermissionMappings_Logins
		FOREIGN KEY (LoginId) REFERENCES Logins(LoginId)
	CONSTRAINT FK_LoginPermissionMappings_Permissions
		FOREIGN KEY (PermissionId) REFERENCES [Permissions](PermissionId)
)
GO

CREATE UNIQUE INDEX UX_LoginPermissionMappings_LoginId_PermissionId
	ON LoginPermissionMappings(LoginId, PermissionId)
	INCLUDE (IsGranted)
GO
