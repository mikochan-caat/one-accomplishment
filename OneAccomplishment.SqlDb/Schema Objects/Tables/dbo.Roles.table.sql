﻿CREATE TABLE [dbo].[Roles]
(
	RoleId bigint NOT NULL IDENTITY(1, 1),
	RoleName varchar(50) NOT NULL,
	IsBuiltIn bit NOT NULL,
	[Description] varchar(256) NOT NULL

	CONSTRAINT PK_Roles
		PRIMARY KEY CLUSTERED (RoleId)

	CONSTRAINT CHK_Roles_RoleName
		CHECK (LEN(LTRIM(RTRIM(RoleName))) > 0)
)
GO

CREATE UNIQUE INDEX UX_Roles_RoleName
	ON [Roles](RoleName)
GO
