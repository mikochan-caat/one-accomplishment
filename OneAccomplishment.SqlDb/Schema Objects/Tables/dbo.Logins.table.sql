﻿CREATE TABLE [dbo].[Logins]
(
	LoginId bigint NOT NULL IDENTITY(1, 1),
	Username varchar(104) NOT NULL				-- Max logon name length, see http://technet.microsoft.com/en-us/library/bb726984.aspx

	CONSTRAINT PK_Logins
		PRIMARY KEY CLUSTERED (LoginId)
	CONSTRAINT UX_Logins_Username
		UNIQUE (Username)

	CONSTRAINT CHK_Logins_Username
		CHECK (LEN(Username) > 0)
)
GO
