﻿using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("One Accomplishment .NET Unit Tests")]
[assembly: AssemblyDescription("TMS task accomplishments generator")]
[assembly: AssemblyProduct("OneAccomplishment")]
[assembly: AssemblyCopyright("Copyright © 2019 ITDCAT")]

[assembly: ComVisible(false)]
[assembly: Guid("4d2cc5cc-b905-44e7-ab95-5b95a04c2a47")]

[assembly: AssemblyVersion("1.0.*")]
