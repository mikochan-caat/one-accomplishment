param($installPath, $toolsPath, $package, $project)

function Remove-ProjectItem($proj, $name) {
    try { $proj.ProjectItems.Item($name).Delete() } catch { }
}

$assemblyName = "Kodeo.Reegenerator"
$targetAssembly = "$($assemblyName).dll"
$targetXmlDoc = "$($assemblyName).xml"

Write-Host "Attempting to delete an instance of the pre-installed Reegenerator assembly..."
Remove-ProjectItem $project $targetAssembly
Remove-ProjectItem $project $targetXmlDoc

Write-Host "Updating embedded interop types for referenced assemblies..."
$assemblyReferencesToModify = @{
    "EnvDTE" = $false;
    "EnvDTE80" = $false;
    "Microsoft.VisualStudio.OLE.Interop" = $false;
    "Microsoft.VisualStudio.Shell.10.0" = $false;
    "Microsoft.VisualStudio.Shell.Interop" = $false;
    "Microsoft.VisualStudio.Shell.Interop.8.0" = $false;
    "stdole" = $false;
    "VSLangProj" = $true;
    "VSLangProj2" = $true;
    "VSLangProj80" = $true;
}
$project.Object.References | %{ 
    $assemblyEmbedInteropTypes = $assemblyReferencesToModify[$_.Name]
    if ($assemblyEmbedInteropTypes -ne $null) {
        $_.EmbedInteropTypes = $assemblyEmbedInteropTypes 
    }
}

Write-Host "Install finished."
