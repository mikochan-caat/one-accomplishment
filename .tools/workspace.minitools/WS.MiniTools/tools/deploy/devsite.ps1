. (Join-Path $PSScriptRoot "dep_init.ps1") @args

# Create Win32 wrappers
# ShowWindow constants
# Hide = 0,
# ShowNormal = 1,
# ShowMinimized = 2,
# ShowMaximized = 3,
# Maximize = 3,
# ShowNormalNoActivate = 4,
# Show = 5,
# Minimize = 6,
# ShowMinNoActivate = 7,
# ShowNoActivate = 8,
# Restore = 9,
# ShowDefault = 10,
# ForceMinimized = 11
Add-Type -Namespace Console -Name Window -MemberDefinition '
[DllImport("user32.dll")]
public static extern long SetForegroundWindow(int hWnd);

[DllImport("kernel32.dll")]
public static extern IntPtr GetConsoleWindow();

[DllImport("user32.dll")]
public static extern bool ShowWindow(IntPtr hWnd, Int32 nCmdShow);
'

function Show-Console {
    $consolePtr = [Console.Window]::GetConsoleWindow()
    [Console.Window]::SetForegroundWindow($consolePtr) | Out-Null
    [Console.Window]::ShowWindow($consolePtr, 1) | Out-Null
}

function Hide-Console {
    $consolePtr = [Console.Window]::GetConsoleWindow()
    [Console.Window]::ShowWindow($consolePtr, 0) | Out-Null
}

$optionsPath = Join-Path $SolutionDir $LocalDeploymentOptionsPath
if (-not(Test-Path -Path $optionsPath)) {
    Write-Warn "The local options file was not found. Invoke Write-DevSiteOptions to create one."
    Wait-Exit
}

Write-Notice "Finding IIS Express..."
$iisExpressInstallPath = Get-ItemProperty "HKLM:\SOFTWARE\Microsoft\IISExpress\*" | `
                         Sort-Object -Descending -Property PSPath | `
                         Select-Object -ExpandProperty InstallPath -First 1
if (-not $iisExpressInstallPath) {
    Write-Warn "Could not find an installation of IIS Express on this system."
    Wait-Exit
}

$iisExpressExe = Join-Path $iisExpressInstallPath "iisexpress.exe"
$options = Get-Content -Path $optionsPath -Encoding "UTF8" | Out-String | ConvertFrom-Json

$OrigBgColor = $host.ui.rawui.BackgroundColor
$OrigFgColor = $host.ui.rawui.ForegroundColor
Hide-Console
try {
    $configName = "applicationhost.config"

    Write-Notice "Configuring website..."
    $tempFolder = Join-Path $SolutionDir $SolutionLocalDeploymentTempFolder
    New-Item -ItemType Directory -Path $tempFolder -Force | Out-Null

    $xmlConfig = [xml](Get-Content -Path ".\$($configName)")
    $siteNode = $xmlConfig.SelectSingleNode("//system.applicationHost/sites/site[@id='1']")
    $appNode = $siteNode.SelectSingleNode("//application[@path='/']/virtualDirectory[@path='/']")
    $bindingsNode = $siteNode.SelectSingleNode("//bindings/binding[@protocol='http']")
    $appNode.SetAttribute("physicalPath", (Join-Path $SolutionDir $SolutionLocalDeploymentFolder))
    $bindingsNode.SetAttribute("bindingInformation", "*:$($options.Port):")

    $tempConfigPath = Join-Path $tempFolder $configName
    $xmlConfig.Save($tempConfigPath)

    Write-Notice "Launching IIS Express..."
    $env:IisExpressArgs = (
        "/config:`"$($tempConfigPath)`""
    ) -join " "
    & $iisExpressExe --% %IisExpressArgs%
    if (-not $?) {
        Write-Error "IIS Express returned a non-successful exit code."
    }
} finally {
    $host.ui.rawui.BackgroundColor = $OrigBgColor
    $host.ui.rawui.ForegroundColor = $OrigFgColor
    Show-Console
}

Write-Host ""
Write-Notice "Process was terminated."
Wait-Pause
