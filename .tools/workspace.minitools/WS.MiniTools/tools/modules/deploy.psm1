. (Join-Path $PSScriptRoot "..\module_init.ps1") @args
. (Join-Path $PSScriptRoot "..\deploy_init.ps1")

function Publish-DevSite {
    Restore-GenFiles
    Invoke-DeploymentScript "depdev.ps1"
}

function Publish-ProdSite {
    Restore-GenFiles
    Invoke-DeploymentScript "depprod.ps1"
}

function Start-DevSite {
    Invoke-DeploymentScript "devsite.ps1"
}

function Write-DevSiteOptions {
    $options = @{
        Port = 10000
    }
    $optionsPath = Join-Path $SolutionDir $LocalDeploymentOptionsPath
    if (-not(Test-Path -Path $optionsPath)) {
        ConvertTo-Json -InputObject $options | `
        Out-File -FilePath $optionsPath `
                 -Encoding "UTF8" `
                 -noClobber 
        Write-NoticeUI "The local options file was created successfully."
    } else {
        Write-NoticeUI "The local options file already exists. No action taken."
    }
    return $options
}

function Invoke-DeploymentScript {
    param (
        [Parameter(Mandatory=$true)][string]$ScriptName
    )
    $currentLocation = Join-Path $ToolsPath "deploy"
    $scriptPath = Join-Path $currentLocation $ScriptName
    $currentConfig = $SolutionNode.Projects.Item(1).ConfigurationManager.ActiveConfiguration.ConfigurationName

    $arguments = (
        "-ExecutionPolicy Bypass", 
        "-File `"$($scriptPath)`"",
        "`"$($ToolsPath)`"",
        "`"$($currentLocation)`"",
        "`"$($currentConfig)`"",
        "`"$($SolutionNode.FullName)`""
    ) -join " "
    Start-Process powershell.exe -Verb RunAs -ArgumentList $arguments
}

function Restore-GenFiles {
    Write-NoticeUI "Creating generated deployment files..."
    $sourceFileName = "$($SolutionDeploymentFolder)\regen.deployfiles.txt"
    $sourceFile = Join-Path $SolutionDir $sourceFileName
    if (-not(Test-Path -Path $sourceFile)) {
        Write-NoticeUI "The file $($sourceFileName) was not found. No action taken."
        Write-WarnUI "Make sure to put regen.deployfiles.txt at the correct folder from the solution root."
        return
    }
    Get-Content $sourceFile | where{ -not([string]::IsNullOrWhiteSpace($_)) -and -not([IO.Path]::IsPathRooted($_)) } | %{
        $fullPath = Join-Path $SolutionDir $_
        $copyFolder = [IO.Path]::GetDirectoryName($fullPath)
        $copyFileName = [IO.Path]::GetFileName($fullPath)
        if (-not(Test-Path -Path $fullPath)) {
            New-Item -Path $copyFolder -Name $copyFileName -Type file -ErrorAction Continue | Out-Null
            Write-Host "Copied '$($_)'."
        } else {
            Write-Host "Skipped '$($_)' (existing)."
        }
    }
}

function Write-SiteAcls {
    Write-NoticeUI "Dumping IIS Website Application ACLs..."

    $iisAppPath = Get-SitePath
    if (-not $iisAppPath) {
        Write-Critical "Could not find an IIS Website Application with the name '$($SiteIisAppName)'."
        return
    }

    $csvPath = Join-Path $SolutionDir $DeploymentPermissionsCsvPath
    
    # Exclude inherited rights from the CSV output
    (Get-Acl $iisAppPath).Access | ?{ !$_.IsInherited } | 
    Select-Object @{ n = 'Path'; e = { $iisAppPath } }, IdentityReference, AccessControlType, `
                  InheritanceFlags, PropagationFlags, FileSystemRights |
    Export-Csv $csvPath -Force

    Write-NoticeUI "IIS Website Application ACLs dumped to '$($csvPath)'."
}

New-Alias -Name "resdep" -Value Restore-GenFiles -Force
New-Alias -Name "getacls" -Value Write-SiteAcls -Force
New-Alias -Name "depdev" -Value Publish-DevSite -Force
New-Alias -Name "depprod" -Value Publish-ProdSite -Force
New-Alias -Name "startdev" -Value Start-DevSite -Force
New-Alias -Name "optdev" -Value Write-DevSiteOptions -Force

Export-ModuleMember -Function Restore-GenFiles -Alias "resdep"
Export-ModuleMember -Function Write-SiteAcls -Alias "getacls"
Export-ModuleMember -Function Publish-DevSite -Alias "depdev"
Export-ModuleMember -Function Publish-ProdSite -Alias "depprod"
Export-ModuleMember -Function Start-DevSite -Alias "startdev"
Export-ModuleMember -Function Write-DevSiteOptions -Alias "optdev"
