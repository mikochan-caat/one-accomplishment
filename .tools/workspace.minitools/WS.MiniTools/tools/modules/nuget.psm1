. (Join-Path $PSScriptRoot "..\module_init.ps1") @args

function Invoke-PostRestore {
    Update-Package -Id JavaScriptEngineSwitcher.ChakraCore.Native.win-x86 -Project OneAccomplishment.Site -Reinstall
    Update-Package -Id JavaScriptEngineSwitcher.ChakraCore.Native.win-x64 -Project OneAccomplishment.Site -Reinstall
}

New-Alias -Name "ngpres" -Value Invoke-PostRestore -Force

Export-ModuleMember -Function Invoke-PostRestore -Alias "ngpres"
