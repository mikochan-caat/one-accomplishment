﻿You have successfully installed the T4MVC package.
This package is a solution-wide package instead of a project-level package.

To begin, type the command in the Package Manager Console:
    Install-T4MVC

For more information, type:
    Get-Help Install-T4MVC
