#Requires -version 3.0
param($InstallPath, $ToolsPath, $Package)
$ErrorActionPreference = 'Stop'

function Register-Module {
    param (
        [Parameter(Mandatory=$true)][string]$ModulePath
    )
    Import-Module (Join-Path "$($ToolsPath)\modules" $ModulePath) `
        -Force `
        -ArgumentList $InstallPath, $ToolsPath, $Package
}

Register-Module "workspace.psm1"

# Copy the lib folder to tools (if it does not exist)
$solutionRoot = Split-Path $dte.Solution.FileName
$toolsLibFolder = Join-Path $ToolsPath "lib"
$linq2dbToolsPath = Join-Path $solutionRoot ".tools\linq2db.t4models"

Get-ChildItem $toolsLibFolder -Filter "*.dll" | %{
    $current = Join-Path $linq2dbToolsPath $_.Name
    if (-not(Test-Path $current -PathType Leaf)) {
        New-Item -ItemType File -Path $current -Force
        Copy-Item $_.FullName $current -Force
    }
}
