﻿You have successfully installed the Linq2DB.SqlServer standalone T4 template package.
This package is a solution-wide package instead of a project-level package.

To begin, type the command in the Package Manager Console:
    Install-Linq2DbTemplates

For more information, type:
    Get-Help Install-Linq2DbTemplates
