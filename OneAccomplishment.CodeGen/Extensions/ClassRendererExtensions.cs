﻿using System.IO;
using Net40Utilities.Validation;
using OneAccomplishment.CodeGen.Core;
using OneAccomplishment.CodeGen.Renderers;

namespace OneAccomplishment.CodeGen.Extensions
{
    internal static class ClassRendererExtensions
    {
        public static void InitializeClassRendererMembers<T>(this T renderer) 
            where T : IRendererProjectItem, IClassRenderer
        {
            ArgumentGuard.For<object>(() => renderer).IsNull().Throw();

            var projectItem = renderer.ProjectItem;
            string itemName = Path.GetFileNameWithoutExtension(projectItem.FullPath);
            renderer.ClassName = itemName;
            renderer.ValidClassName = CSharpLanguage.CreateValidIdentifier(itemName);
        }
    }
}
