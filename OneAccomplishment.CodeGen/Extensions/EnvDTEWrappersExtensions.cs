﻿using System.Collections.Generic;
using System.Collections.Immutable;
using System.IO;
using System.Linq;
using Kodeo.Reegenerator.Wrappers;
using Net40Utilities.Validation;

namespace OneAccomplishment.CodeGen.Extensions
{
    internal static class EnvDTEWrappersExtensions
    {
        private static readonly char[] PathTrimChars = new char[] {
            Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar
        };

        public static IReadOnlyList<string> TransformPaths(
            this Project project, IEnumerable<string> projectPaths)
        {
            ArgumentGuard.For(() => project).IsNull().Throw();
            ArgumentGuard.For(() => projectPaths).IsNull().Throw();

            string projectDir = project.FullPath;
            var projectPathsEnumerable = projectPaths
                .Select(path => Path.Combine(projectDir, path).TrimEnd(PathTrimChars));
            return ImmutableList.CreateRange(projectPathsEnumerable);
        }
    }
}
