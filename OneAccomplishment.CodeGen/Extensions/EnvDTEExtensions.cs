﻿using System.Collections.Generic;
using EnvDTE;
using Net40Utilities.Validation;

namespace OneAccomplishment.CodeGen.Extensions
{
    internal static class EnvDTEHelpers
    {
        public static IEnumerable<ProjectItem> GetAllProjectItems(this Project project)
        {
            ArgumentGuard.For(() => project).IsNull().Throw();

            return GetProjectItemsRecursively(project.ProjectItems);
        }

        private static IEnumerable<ProjectItem> GetProjectItemsRecursively(ProjectItems items)
        {
            if (items == null) {
                yield break;
            }

            foreach (ProjectItem projectItem in items) {
                yield return projectItem;

                var subItems = GetProjectItemsRecursively(projectItem.ProjectItems);
                foreach (ProjectItem subProjectItem in subItems) {
                    yield return subProjectItem;
                }
            }
        }
    }
}
