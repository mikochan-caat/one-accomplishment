﻿using System;

namespace OneAccomplishment.CodeGen.Core
{
    internal static class Namespaces
    {
        private const string RootNamespace = "OneAccomplishment.Core";

        public static string FromRoot(string subNamespace = null)
        {
            if (String.IsNullOrWhiteSpace(subNamespace)) {
                return RootNamespace;
            }
            return RootNamespace + "." + subNamespace;
        }
    }
}
