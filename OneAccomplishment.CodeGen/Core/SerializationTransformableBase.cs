﻿using System.Runtime.Serialization;

namespace OneAccomplishment.CodeGen.Core
{
    public abstract class SerializationTransformableBase
    {
        [OnDeserialized]
        private void OnDeserialization(StreamingContext context)
        {
            this.OnDeserializationCore();
        }

        protected abstract void OnDeserializationCore();
    }
}
