﻿using System;
using System.Linq;
using OneAccomplishment.CodeGen.Core;

namespace OneAccomplishment.CodeGen.Data
{
    internal sealed class T4Configuration
    {
        public static readonly T4Configuration Instance = new T4Configuration();

        public string DatabaseServer { get; private set; }
        public string DatabaseCatalog { get; private set; }
        public string NamespaceName { get; private set; }
        public string ExcludedProcedures { get; private set; }

        private T4Configuration()
        {
            this.DatabaseServer = @"SqlServer\OneAccomplishment";
            this.DatabaseCatalog = @"OneAccomplishment.SqlDb_DEV";
            this.NamespaceName = Namespaces.FromRoot("Data");

            var excludedProcedureItems = (new[] {
                StoredProcs.Configurations_GetDescriptions,
                StoredProcs.Permissions_GetEnumMetadata
            }).Select(proc =>  "\"" + proc + "\"");
            this.ExcludedProcedures = String.Join("," + Environment.NewLine, excludedProcedureItems);
        }
    }
}
