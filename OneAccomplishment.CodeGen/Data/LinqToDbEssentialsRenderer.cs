using System;
using System.IO;
using Kodeo.Reegenerator.Generators;
using Kodeo.Reegenerator.Wrappers;
using Microsoft.VisualStudio.Shell;
using Newtonsoft.Json;
using OneAccomplishment.CodeGen.Data.Essentials;
using OneAccomplishment.CodeGen.Extensions;
using OneAccomplishment.CodeGen.Renderers;

namespace OneAccomplishment.CodeGen.Data
{
    [TemplateDisplay(
        DisplayName = "Linq2DB Essentials Renderer", 
        Description = "Renders the essential OneAccomplishment database classes and interfaces.")]
    [DefaultExtension(Extension = ".essentials.log")]
    public sealed class LinqToDbEssentialsRenderer : CodeRenderer, IRendererOutput, IRendererServices
    {
        OutputPaneTraceListener IRendererServices.OutputPaneTraceListener
        {
            get { return this.OutputPaneTraceListener; }
        }

        ServiceProvider IRendererServices.SiteServiceProvider
        {
            get { return this.SiteServiceProvider; }
        }

        ProjectItem IRendererProjectItem.ProjectItem
        {
            get { return this.ProjectItem; }
        }

        StringWriter IRendererOutput.Output
        {
            get { return this.Output; }
        }

        public override RenderResults Render()
        {
            this.RenderHeader();
            var metadata = this.GenerateMetadata();
            this.Output.WriteLine("// Generated metadata.");
            this.Output.WriteLine();

            // Start rendering the files
            this.Output.Write((new ConfigurationsRenderer(this, metadata)).RenderNestedContent());
            this.Output.WriteLine("// Generated configuration classes and interfaces.");
            this.Output.WriteLine();
            this.Output.Write((new PermissionEnumsRenderer(this, metadata)).RenderNestedContent());
            this.Output.WriteLine("// Generated permission enumerations.");
            this.Output.WriteLine();

            return new RenderResults(this.Output.ToString());
        }

        private LinqToDbMetadata GenerateMetadata()
        {
            string metadataTemplate = (new LinqToDbMetadataRenderer(this)).RenderNestedContent();
            var generatedJsonBlob = this.RunOtherCustomTool(
                Generators.TextTemplatingFileGenerator,
                this.ProjectItem.FullPath,
                metadataTemplate,
                this.ProjectItem.CodeNamespace);
            string jsonString = RenderResults.DefaultEncoding.GetString(generatedJsonBlob);
            var serializerSettings = new JsonSerializerSettings {
                ConstructorHandling = ConstructorHandling.AllowNonPublicDefaultConstructor
            };
            return JsonConvert.DeserializeObject<LinqToDbMetadata>(jsonString, serializerSettings);
        }

        object IRendererServices.GetService(Guid serviceGuid)
        {
            return this.GetService(serviceGuid);
        }

        object IRendererServices.GetService(Type serviceType)
        {
            return this.GetService(serviceType);
        }
    }
}
