using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using Kodeo.Reegenerator.Wrappers;
using LinqToDB.Data;
using OneAccomplishment.CodeGen.Core;
using OneAccomplishment.CodeGen.Extensions;
using OneAccomplishment.CodeGen.Renderers;

namespace OneAccomplishment.CodeGen.Data.Essentials
{
    public partial class PermissionEnumsRenderer : IRendererProjectItem, IRendererOutput
    {
        private readonly LinqToDbMetadata metadata;

        public string PermissionsNamespace { get; private set; }
        public IEnumerable<PermissionCategoryInfo> PermissionCategories { get; private set; }

        ProjectItem IRendererProjectItem.ProjectItem
        {
            get { return this.ParentRenderer.ProjectItem; }
        }

        StringWriter IRendererOutput.Output
        {
            get { return this.Output; }
        }

        public PermissionEnumsRenderer(IRendererServices parentRenderer, LinqToDbMetadata metadata)
            : base(parentRenderer)
        {
            this.metadata = metadata;
        }

        public override void PreRender()
        {
            base.PreRender();
            this.RenderHeader();

            this.PermissionsNamespace = Namespaces.FromRoot("Services.Auth");
            
            var permissionCategories = new List<PermissionCategoryInfo>();
            using (var connection = LinqToDbHelpers.GetSqlServerConnection())
            using (var readerWrapper = this.CreatePermissionEnumMetadataDataReader(connection)) {
                var reader = readerWrapper.Reader;
                while (reader.Read()) {
                    string currentCategory = reader.GetString(reader.GetOrdinal("CurrentCategory"));
                    var category = new PermissionCategoryInfo {
                        Category = currentCategory,
                        ValidCategory = CSharpLanguage.CreateValidIdentifier(currentCategory)
                    };

                    if (!reader.NextResult()) {
                        throw new InvalidOperationException("No more result sets found.");
                    }

                    while (reader.Read()) {
                        string name = reader.GetString(reader.GetOrdinal("Name"));
                        long value = reader.GetInt64(reader.GetOrdinal("Value"));
                        string description = reader.GetString(reader.GetOrdinal("Description"));
                        category.Permissions.Add(new Permission {
                            Name = name,
                            ValidName = CSharpLanguage.CreateValidIdentifier(name),
                            Value = value,
                            Description = description
                        });
                    }
                    permissionCategories.Add(category);

                    reader.NextResult();
                }
            }

            this.PermissionCategories = permissionCategories;
        }

        private DataReader CreatePermissionEnumMetadataDataReader(DataConnection connection)
        {
            return connection.ExecuteReader(
                StoredProcs.Permissions_GetEnumMetadata,
                CommandType.StoredProcedure,
                CommandBehavior.Default);
        }

        public sealed class PermissionCategoryInfo
        {
            public string Category { get; set; }
            public string ValidCategory { get; set; }
            public IList<Permission> Permissions { get; private set; }

            public PermissionCategoryInfo()
            {
                this.Permissions = new List<Permission>();
            }
        }

        public sealed class Permission
        {
            public string Name { get; set; }
            public string ValidName { get; set; }
            public long Value { get; set; }
            public string Description { get; set; }
        }
    }
}
