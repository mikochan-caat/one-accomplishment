using Newtonsoft.Json;
using OneAccomplishment.CodeGen.Renderers;

namespace OneAccomplishment.CodeGen.Data.Essentials
{
    public sealed partial class LinqToDbMetadataRenderer
    {
        public string JsonNetAssemblyPath { get; private set; }
        public string DatabaseServer { get; private set; }
        public string DatabaseCatalog { get; private set; }

        public LinqToDbMetadataRenderer(IRendererServices parentRenderer)
            : base(parentRenderer)
        {
        }

        public override void PreRender()
        {
            base.PreRender();
            this.JsonNetAssemblyPath = typeof(JsonConvert).Assembly.Location;
            this.DatabaseServer = T4Configuration.Instance.DatabaseServer;
            this.DatabaseCatalog = T4Configuration.Instance.DatabaseCatalog;
        }
    }
}
