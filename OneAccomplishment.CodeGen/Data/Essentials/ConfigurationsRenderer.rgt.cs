using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using Kodeo.Reegenerator.Wrappers;
using LinqToDB.Data;
using OneAccomplishment.CodeGen.Core;
using OneAccomplishment.CodeGen.Extensions;
using OneAccomplishment.CodeGen.Renderers;

namespace OneAccomplishment.CodeGen.Data.Essentials
{
    public sealed partial class ConfigurationsRenderer : IRendererProjectItem, IRendererOutput
    {
        private readonly LinqToDbMetadata metadata;

        public string ServiceNamespace { get; private set; }
        public string ConfigNamespace { get; private set; }
        public IEnumerable<ConfigValue> ConfigValues { get; private set; }
        public IDictionary<string, string> ConfigDescriptions { get; private set; }

        ProjectItem IRendererProjectItem.ProjectItem
        {
            get { return this.ParentRenderer.ProjectItem; }
        }

        StringWriter IRendererOutput.Output
        {
            get { return this.Output; }
        }

        public ConfigurationsRenderer(IRendererServices parentRenderer, LinqToDbMetadata metadata)
            : base(parentRenderer)
        {
            this.metadata = metadata;
        }

        public override void PreRender()
        {
            base.PreRender();
            this.RenderHeader();

            this.ServiceNamespace = Namespaces.FromRoot("Services");
            this.ConfigNamespace = Namespaces.FromRoot("Services.Configuration");
            this.ConfigValues = this.metadata
                .Procedures[StoredProcs.Configurations_GetValues].ResultTable.Columns
                .Select(col => new ConfigValue {
                    MemberName = col.Value.MemberName,
                    ValidMemberName = LinqToDbHelpers.ToValidName(col.Value.MemberName),
                    Type = LinqToDbHelpers.StripNullableFromType(col.Value.Type)
                })
                .ToArray();
            this.ConfigDescriptions = new Dictionary<string, string>();

            using (var connection = LinqToDbHelpers.GetSqlServerConnection())
            using (var readerWrapper = this.CreateConfigDescriptionsDataReader(connection)) {
                var reader = readerWrapper.Reader;
                while (reader.Read()) {
                    string name = reader.GetString(reader.GetOrdinal("Name"));
                    string description = reader.GetString(reader.GetOrdinal("Description"));
                    this.ConfigDescriptions[name] = description;
                }
            }
        }

        private DataReader CreateConfigDescriptionsDataReader(DataConnection connection)
        {
            return connection.ExecuteReader(
                StoredProcs.Configurations_GetDescriptions,
                CommandType.StoredProcedure,
                CommandBehavior.Default);
        }

        public sealed class ConfigValue
        {
            public string MemberName { get; set; }
            public string ValidMemberName { get; set; }
            public string Type { get; set; }
        }
    }
}
