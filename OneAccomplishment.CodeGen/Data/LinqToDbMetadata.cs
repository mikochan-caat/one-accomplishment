﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Data;
using Newtonsoft.Json;
using OneAccomplishment.CodeGen.Core;

namespace OneAccomplishment.CodeGen.Data
{
    public sealed class LinqToDbMetadata : SerializationTransformableBase
    {
        [JsonProperty(PropertyName = "Tables")]
        private Dictionary<string, Table> _tables = null;
        [JsonProperty(PropertyName = "Procedures")]
        private Dictionary<string, Procedure> _procedures = null;

        [JsonIgnore]
        public IReadOnlyDictionary<string, Table> Tables { get; private set; }
        [JsonIgnore]
        public IReadOnlyDictionary<string, Procedure> Procedures { get; private set; }

        private LinqToDbMetadata() { }

        protected override void OnDeserializationCore()
        {
            this.Tables = ToReadOnly(this._tables);
            this.Procedures = ToReadOnly(this._procedures);
        }

        private static IReadOnlyList<T> ToReadOnly<T>(List<T> list)
        {
            if (list == null) {
                return null;
            }
            return ImmutableList.CreateRange(list);
        }

        private static IReadOnlyDictionary<TKey, TValue> ToReadOnly<TKey, TValue>(
           Dictionary<TKey, TValue> dictionary)
        {
            if (dictionary == null) {
                return null;
            }
            return ImmutableDictionary.CreateRange(dictionary);
        }

        public sealed class Table : SerializationTransformableBase
        {
            [JsonProperty]
            public string Schema { get; private set; }
            [JsonProperty]
            public string TableName { get; private set; }
            [JsonProperty]
            public string DataContextPropertyName { get; private set; }
            [JsonProperty]
            public string Description { get; private set; }
            [JsonProperty]
            public string AliasPropertyName { get; private set; }
            [JsonProperty]
            public string AliasTypeName { get; private set; }
            [JsonProperty]
            public string TypeName { get; private set; }
            [JsonProperty]
            public bool IsView { get; private set; }

            [JsonProperty(PropertyName = "Columns")]
            private Dictionary<string, Column> _columns = null;
            [JsonProperty(PropertyName = "ForeignKeys")]
            private Dictionary<string, ForeignKey> _foreignKeys = null;

            [JsonIgnore]
            public IReadOnlyDictionary<string, Column> Columns { get; private set; }
            [JsonIgnore]
            public IReadOnlyDictionary<string, ForeignKey> ForeignKeys { get; private set; }

            private Table() { }

            protected override void OnDeserializationCore()
            {
                this.Columns = ToReadOnly(this._columns);
                this.ForeignKeys = ToReadOnly(this._foreignKeys);
            }
        }

        public sealed class Column : Property
        {
            [JsonProperty]
            public string ColumnName { get; private set; } // Column name in database
            [JsonProperty]
            public string ColumnType { get; private set; } // Type of the column in database
            [JsonProperty]
            public string AliasName { get; private set; }
            [JsonProperty]
            public string MemberName { get; private set; }
            [JsonProperty]
            public string Description { get; private set; }
            [JsonProperty]
            public bool IsNullable { get; private set; }
            [JsonProperty]
            public bool IsIdentity { get; private set; }
            [JsonProperty]
            public bool IsPrimaryKey { get; private set; }
            [JsonProperty]
            public bool SkipOnUpdate { get; private set; }
            [JsonProperty]
            public bool SkipOnInsert { get; private set; }
            [JsonProperty]
            public bool IsDuplicateOrEmpty { get; private set; }
            [JsonProperty]
            public int PrimaryKeyOrder { get; private set; }
            [JsonProperty]
            public DbType DbType { get; private set; }

            private Column() { }
        }

        public sealed class ForeignKey : Property
        {
            [JsonProperty]
            public bool CanBeNull { get; private set; }
            [JsonProperty]
            public string KeyName { get; private set; }
            [JsonProperty]
            public string MemberName { get; private set; }
            [JsonProperty]
            public Table OtherTable { get; private set; }
            [JsonProperty]
            public ForeignKey BackReference { get; private set; }
            [JsonProperty]
            public AssociationType AssociationType { get; private set; }

            [JsonProperty(PropertyName = "ThisColumns")]
            private List<Column> _thisColumns = null;
            [JsonProperty(PropertyName = "OtherColumns")]
            private List<Column> _otherColumns = null;

            [JsonIgnore]
            public IReadOnlyList<Column> ThisColumns { get; private set; }
            [JsonIgnore]
            public IReadOnlyList<Column> OtherColumns { get; private set; }

            private ForeignKey() { }

            protected override void OnDeserializationCore()
            {
                base.OnDeserializationCore();
                this.ThisColumns = ToReadOnly(this._thisColumns);
                this.OtherColumns = ToReadOnly(this._otherColumns);
            }
        }

        public sealed class Procedure : Method
        {
            [JsonProperty]
            public string Schema { get; private set; }
            [JsonProperty]
            public string ProcedureName { get; private set; }
            [JsonProperty]
            public bool IsFunction { get; private set; }
            [JsonProperty]
            public bool IsTableFunction { get; private set; }
            [JsonProperty]
            public bool IsDefaultSchema { get; private set; }

            [JsonProperty]
            public Table ResultTable { get; private set; }
            [JsonProperty]
            public Exception ResultException { get; private set; }

            [JsonProperty(PropertyName = "SimilarTables")]
            private List<Table> _similarTables = null;
            [JsonProperty(PropertyName = "ProcParameters")]
            private List<Parameter> _procParameters = null;

            [JsonIgnore]
            public IReadOnlyList<Table> SimilarTables { get; private set; }
            [JsonIgnore]
            public IReadOnlyList<Parameter> ProcParameters { get; private set; }

            private Procedure() { }

            protected override void OnDeserializationCore()
            {
                base.OnDeserializationCore();
                this.SimilarTables = ToReadOnly(this._similarTables);
                this.ProcParameters = ToReadOnly(this._procParameters);
            }
        }

        public sealed class Parameter
        {
            [JsonProperty]
            public string SchemaName { get; private set; }
            [JsonProperty]
            public string SchemaType { get; private set; }
            [JsonProperty]
            public string ParameterName { get; private set; }
            [JsonProperty]
            public string ParameterType { get; private set; }
            [JsonProperty]
            public string DataType { get; private set; }
            [JsonProperty]
            public bool IsIn { get; private set; }
            [JsonProperty]
            public bool IsOut { get; private set; }
            [JsonProperty]
            public bool IsResult { get; private set; }
            [JsonProperty]
            public int? Size { get; private set; }
            [JsonProperty]
            public Type SystemType { get; private set; }

            private Parameter() { }
        }

        public abstract class Property : MemberBase
        {
            [JsonProperty]
            public bool IsAuto { get; private set; }
            [JsonProperty]
            public bool IsVirtual { get; private set; }
            [JsonProperty]
            public bool IsOverride { get; private set; }
            [JsonProperty]
            public bool IsAbstract { get; private set; }
            [JsonProperty]
            public bool IsStatic { get; private set; }
            [JsonProperty]
            public bool HasGetter { get; private set; }
            [JsonProperty]
            public bool HasSetter { get; private set; }
            [JsonProperty]
            public string InitValue { get; private set; }

            [JsonProperty]
            public int GetterLen { get; private set; }
            [JsonProperty]
            public int SetterLen { get; private set; }

            [JsonProperty(PropertyName = "GetBody")]
            private List<string> _getBody = null;
            [JsonProperty(PropertyName = "SetBody")]
            private List<string> _setBody = null;

            [JsonIgnore]
            public IReadOnlyList<string> GetBody { get; private set; }
            [JsonIgnore]
            public IReadOnlyList<string> SetBody { get; private set; }

            protected Property() { }

            protected override void OnDeserializationCore()
            {
                base.OnDeserializationCore();
                this.GetBody = ToReadOnly(this._getBody);
                this.SetBody = ToReadOnly(this._setBody);
            }
        }

        public abstract class Method : MemberBase
        {
            [JsonProperty]
            public bool IsAbstract { get; private set; }
            [JsonProperty]
            public bool IsVirtual { get; private set; }
            [JsonProperty]
            public bool IsOverride { get; private set; }
            [JsonProperty]
            public bool IsStatic { get; private set; }

            [JsonProperty(PropertyName = "AfterSignature")]
            private List<string> _afterSignature = null;
            [JsonProperty(PropertyName = "Parameters")]
            private List<string> _parameters = null;
            [JsonProperty(PropertyName = "Body")]
            private List<string> _body = null;

            [JsonIgnore]
            public IReadOnlyList<string> AfterSignature { get; private set; }
            [JsonIgnore]
            public IReadOnlyList<string> Parameters { get; private set; }
            [JsonIgnore]
            public IReadOnlyList<string> Body { get; private set; }

            protected Method() { }

            protected override void OnDeserializationCore()
            {
                base.OnDeserializationCore();
                this.AfterSignature = ToReadOnly(this._afterSignature);
                this.Parameters = ToReadOnly(this._parameters);
                this.Body = ToReadOnly(this._body);
            }
        }

        public abstract class MemberBase : SerializationTransformableBase
        {
            [JsonProperty]
            public string ID { get; private set; }
            [JsonProperty]
            public string Name { get; private set; }
            [JsonProperty]
            public string Type { get; private set; }
            [JsonProperty]
            public string EndLineComment { get; private set; }
            [JsonProperty]
            public string Conditional { get; private set; }
            [JsonProperty]
            public bool InsertBlankLineAfter { get; private set; }
            [JsonProperty]
            public AccessModifier AccessModifier { get; private set; }

            [JsonProperty]
            public int AccessModifierLen { get; private set; }
            [JsonProperty]
            public int ModifierLen { get; private set; }
            [JsonProperty]
            public int TypeLen { get; private set; }
            [JsonProperty]
            public int NameLen { get; private set; }
            [JsonProperty]
            public int ParamLen { get; private set; }
            [JsonProperty]
            public int BodyLen { get; private set; }

            [JsonProperty(PropertyName = "Comment")]
            private List<string> _comment = null;
            [JsonProperty(PropertyName = "Attributes")]
            private List<Attribute> _attributes = null;

            [JsonIgnore]
            public IReadOnlyList<string> Comment { get; private set; }
            [JsonIgnore]
            public IReadOnlyList<Attribute> Attributes { get; private set; }

            protected MemberBase() { }

            protected override void OnDeserializationCore()
            {
                this.Comment = ToReadOnly(this._comment);
                this.Attributes = ToReadOnly(this._attributes);
            }
        }

        public enum AssociationType
        {
            Auto,
            OneToOne,
            OneToMany,
            ManyToOne,
        }

        public enum AccessModifier
        {
            Public,
            Protected,
            Internal,
            Private,
            Partial
        }
    }
}
