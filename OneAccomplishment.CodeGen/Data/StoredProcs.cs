﻿namespace OneAccomplishment.CodeGen.Data
{
    internal static class StoredProcs
    {
        public const string Configurations_GetValues = "Configurations_GetValues";
        public const string Configurations_GetDescriptions = "Configurations_GetDescriptions";
        public const string Permissions_GetEnumMetadata = "Permissions_GetEnumMetadata";
    }
}
