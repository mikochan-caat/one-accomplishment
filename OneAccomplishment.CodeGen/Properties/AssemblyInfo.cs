﻿using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("One Accomplishment .NET Core Logic Library")]
[assembly: AssemblyDescription("TMS task accomplishments generator")]
[assembly: AssemblyProduct("OneAccomplishment")]
[assembly: AssemblyCopyright("Copyright © 2019 ITDCAT")]

[assembly: ComVisible(false)]
[assembly: Guid("500f662b-250a-4992-a926-5bd1c1037a9d")]

[assembly: AssemblyVersion("1.0.*")]
