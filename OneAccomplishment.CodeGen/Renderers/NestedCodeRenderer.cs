﻿using System;
using System.IO;
using Kodeo.Reegenerator.Generators;
using Net40Utilities.Validation;

namespace OneAccomplishment.CodeGen.Renderers
{
    public abstract class NestedCodeRenderer : ICodeRenderer
    {
        protected StringWriter Output { get; private set; }
        protected IRendererServices ParentRenderer { get; private set; }

        public NestedCodeRenderer(IRendererServices parentRenderer)
        {
            ArgumentGuard.For(() => parentRenderer).IsNull().Throw();

            this.Output = new StringWriter();
            this.ParentRenderer = parentRenderer;
        }

        public virtual void PreRender()
        {
        }

        public virtual void PostRender()
        {
        }

        public string RenderNestedContent()
        {
            var renderResults = this.RenderNested();
            return renderResults.Encoding.GetString(renderResults.GeneratedCode);
        }

        public void RenderNested(string generatedCodeExtension)
        {
            var renderResults = this.RenderNested();
            this.ParentRenderer.ProjectItem.ApplyResults(renderResults, generatedCodeExtension, true);
        }

        public RenderResults RenderNested()
        {
            this.PreRender();
            try {
                return this.Render();
            } finally {
                this.PostRender();
            }
        }

        public void SetSite(object site, CustomToolExecutionContext context)
        {
            throw new NotSupportedException();
        }

        public abstract RenderResults Render();
    }
}
