﻿using System;
using Kodeo.Reegenerator.Generators;
using Microsoft.VisualStudio.Shell;

namespace OneAccomplishment.CodeGen.Renderers
{
    public interface IRendererServices : IRendererProjectItem
    {
        OutputPaneTraceListener OutputPaneTraceListener { get; }
        ServiceProvider SiteServiceProvider { get; }

        object GetService(Guid serviceGuid);
        object GetService(Type serviceType);
    }
}
