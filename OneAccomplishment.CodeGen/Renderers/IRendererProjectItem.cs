﻿using Kodeo.Reegenerator.Wrappers;

namespace OneAccomplishment.CodeGen.Renderers
{
    public interface IRendererProjectItem
    {
        ProjectItem ProjectItem { get; }
    }
}
