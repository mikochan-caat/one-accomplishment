﻿using System.Web.Mvc;
using OneAccomplishment.Site.Core.Mvc.Attributes;

namespace OneAccomplishment.Site.Controllers
{
    public partial class DefaultController : Controller
    {
        [NoReturnUrl]
        public virtual ActionResult Index()
        {
            return this.RedirectToAction(MVC.Home.ActionNames.Index, MVC.Home.Name);
        }
    }
}
