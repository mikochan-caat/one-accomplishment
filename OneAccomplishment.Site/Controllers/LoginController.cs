﻿using System.Web.Mvc;
using OneAccomplishment.Core.Aggregates;
using OneAccomplishment.Core.BusinessRules;
using OneAccomplishment.Core.BusinessRules.Violations;
using OneAccomplishment.Core.Models;
using OneAccomplishment.Site.Core.Extensions;
using OneAccomplishment.Site.Core.Mvc.Attributes;
using OneAccomplishment.Site.Core.Services.Auth;
using OneAccomplishment.Site.ViewModels;

namespace OneAccomplishment.Site.Controllers
{
    [AllowAnonymous]
    public partial class LoginController : Controller
    {
        private readonly LoginAggregate loginAggregate;
        private readonly IHttpAuthenticationService httpAuthService;

        public LoginController(
            LoginAggregate loginAggregate, IHttpAuthenticationService httpAuthService)
        {
            this.loginAggregate = loginAggregate;
            this.httpAuthService = httpAuthService;
        }

        [DisableAnonymousCache]
        public virtual ActionResult Index()
        {
            if (this.Request.IsAuthenticated) {
                return this.RedirectFromLoginPage();
            }

            var modelState = this.TempData["ModelState"] as ModelStateDictionary;
            var model = this.TempData["Model"] as LoginViewModel;
            if (modelState != null && model != null) {
                this.ModelState.Merge(modelState);
                return this.View(model);
            }

            return this.View(new LoginViewModel());
        }

        [DisableAnonymousCache]
        public virtual ActionResult Reauthenticate(string username)
        {
            if (this.Request.IsAuthenticated) {
                return this.RedirectFromLoginPage();
            }

            return this.View(MVC.Login.Views.Index, new LoginViewModel {
                Username = username
            });
        }

        [HttpPost]
        public virtual ActionResult Index(LoginViewModel loginVm)
        {
            bool isLoginSuccessful = this.ModelState.IsValid;
            if (isLoginSuccessful) {
                try {
                    this.loginAggregate.Authenticate(new Login {
                        Username = loginVm.Username,
                        Password = loginVm.Password,
                        IsStayLoggedIn = loginVm.IsStayLoggedIn
                    });
                } catch (BusinessRuleViolationException<LoginViolations> ex) {
                    isLoginSuccessful = false;
                    this.ModelState.AddBusinessRuleViolation(ex);
                }
            }

            if (!isLoginSuccessful) {
                loginVm.Password = null;
                this.TempData["ModelState"] = this.ModelState;
                this.TempData["Model"] = loginVm;
                return this.RedirectToAction(MVC.Login.ActionNames.Index);
            }
            return this.RedirectFromLoginPage();
        }

        private ActionResult RedirectFromLoginPage()
        {
            return this.Redirect(this.httpAuthService.GetAuthenticationRedirectUrl());
        }
    }
}
