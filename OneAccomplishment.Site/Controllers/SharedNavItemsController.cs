﻿using System.Web.Mvc;
using OneAccomplishment.Core.Services;
using OneAccomplishment.Core.Services.Auth;
using OneAccomplishment.Site.Core.Mvc;
using OneAccomplishment.Site.ViewModels.Shared;

namespace OneAccomplishment.Site.Controllers
{
    [ChildActionOnly]
    public partial class SharedNavItemsController : Controller
    {
        private readonly IPrincipalService principalService;

        public SharedNavItemsController(IPrincipalService principalService)
        {
            this.principalService = principalService;
        }

        [PermissionFilter(Permissions.Administration.AdministratorClaim)]
        public virtual ActionResult AdministrationMenu()
        {
            var permissions = this.principalService.CurrentPrincipal.Permissions;
            var model = ModelPermissionFilters.FilterModelBooleans(
                new NavItemsViewModel.AdministrationMenuViewModel(),
                permissions);
            return this.PartialView(MVC.Shared.Views.Controls._AdministrationMenu, model);
        }
    }
}
