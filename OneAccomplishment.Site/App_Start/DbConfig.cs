﻿using System.Configuration;
using LinqToDB.Data;

namespace OneAccomplishment.Site
{
    public static class DbConfig
    {
        private const string DbConfigName = "OneAccomplishmentDb";

        public static void Configure()
        {
            DataConnection.DefaultConfiguration = DbConfigName;
            DataConnection.DefaultDataProvider = ConfigurationManager.ConnectionStrings[DbConfigName].ProviderName;
        }
    }
}
