﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web.Optimization;
using BundleTransformer.Core.Bundles;
using BundleTransformer.Core.Orderers;
using BundleTransformer.Core.Resolvers;
using BundleTransformer.Core.Transformers;
using Fasterflect;
using Links;
using OneAccomplishment.Site.Bundling;

namespace OneAccomplishment.Site
{
    public sealed class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            SetupCustomBundleResolver();

            CreateBundle<CustomScriptBundle>(bundles, Bundles.Core.Scripts)
                .Include(SiteBundles.Scripts.Core.Assets.tslib_js)
                .Include(SiteBundles.Scripts.Core.Assets.require_modules_generated_ts)
                .Include(SiteBundles.Scripts.Core.Assets.require_js)
                .Include(SiteBundles.Scripts.Core.Assets.require_config_generated_ts)
                .Include(SiteBundles.Scripts.Core.Assets.require_config_ts)
                .Include(SiteBundles.Scripts.Core.Assets.Main_ts);

            CreateBundle<CustomScriptBundle>(bundles, Bundles.Vendor.Modules)
                .IncludeDirectory(SiteScripts.Vendor.UrlPath, "*.js", true)
                .IncludeDirectory(SiteScripts.Vendor.UrlPath, "*.ts", true);

            CreateBundle<CustomScriptBundle>(bundles, Bundles.Site.Modules)
                .IncludeDirectory(SiteScripts.Helpers.UrlPath, "*.ts", true)
                .IncludeDirectory(SiteScripts.Site.UrlPath, "*.ts", true);

            CreateBundle<CustomStyleBundle>(bundles, Bundles.Site.Styles, SetupSiteStyleBundle)
                .Include(SiteBundles.Content.Styles.Assets._FallbackInit_less)
                .IncludeDirectory(SiteContent.Styles.UrlPath, "*.css", true)
                .IncludeDirectory(SiteContent.Styles.UrlPath, "*.less", true);

            CreateBundle<CustomStyleBundle>(bundles, Bundles.Site.FallbackStyles)
                .Include(SiteBundles.Content.Styles_Fallback.Assets._FallbackInit_less)
                .IncludeDirectory(SiteContent.Styles_Fallback.UrlPath, "*.css", true)
                .IncludeDirectory(SiteContent.Styles_Fallback.UrlPath, "*.less", true);

            CreateBundle<CustomStyleBundle>(bundles, Bundles.Vendor.Styles, SetupVendorStyleBundle)
                .IncludeDirectory(SiteContent.Fonts.UrlPath, "*.css", true)
                .IncludeDirectory(SiteContent.Fonts.UrlPath, "*.less", true)
                .IncludeDirectory(SiteContent.Vendor.UrlPath, "*.css", false)
                .IncludeDirectory(SiteContent.Vendor.UrlPath, "*.less", false)
                .IncludeDirectory(SiteContent.Vendor.Tippy.UrlPath, "*.css", true)
                .IncludeDirectory(SiteContent.Vendor.Tippy.UrlPath, "*.less", true)
                .Include(SiteBundles.Content.Vendor.FontAwesome.Assets.fontawesome_less)
                .Include(SiteBundles.Content.Vendor.FontAwesome.Assets.brands_less)
                .Include(SiteBundles.Content.Vendor.FontAwesome.Assets.regular_less)
                .Include(SiteBundles.Content.Vendor.FontAwesome.Assets.light_less)
                .Include(SiteBundles.Content.Vendor.FontAwesome.Assets.solid_less);
        }

        private static void SetupSiteStyleBundle(CustomStyleBundle styleBundle)
        {
            styleBundle.Transforms.Clear();
            var hardcodedIgnorePaths = new[] {
                SiteBundles.Content.Styles.Assets._Branding_less,
                SiteBundles.Content.Styles.Assets._Controls_less,
            };
            var dynamicIgnorePaths = GetUnderscorePathsFromAssetTypes(new[] {
                typeof(SiteBundles.Content.Styles._Controls.Assets),
            });
            var ignorePatterns = CreateIgnorePatterns(hardcodedIgnorePaths.Concat(dynamicIgnorePaths));
            styleBundle.Transforms.Add(new StyleTransformer(ignorePatterns));
        }

        private static void SetupVendorStyleBundle(CustomStyleBundle styleBundle)
        {
            styleBundle.Transforms.Clear();
            var ignorePatterns = CreateIgnorePatterns(new[] {
                SiteBundles.Content.Vendor.Assets.bootstrap_vars_less,
            });
            styleBundle.Transforms.Add(new StyleTransformer(ignorePatterns));
        }

        [Conditional("DEBUG")]
        private static void SetupCustomBundleResolver()
        {
            BundleResolver.Current = new CustomBundleResolver();
        }

        private static T CreateBundle<T>(BundleCollection bundleCollection, string bundleName, Action<T> initAction = null) where T : Bundle
        {
            var bundle = (T)Activator.CreateInstance(typeof(T), bundleName);
            bundle.Orderer = new NullOrderer();
            if (initAction != null) {
                initAction(bundle);
            }
            bundleCollection.Add(bundle);
            return bundle;
        }

        private static string[] CreateIgnorePatterns(IEnumerable<string> ignorePatterns)
        {
            return ignorePatterns
                .Where(p => !String.IsNullOrEmpty(p))
                .Select(p => p.Substring(1))
                .ToArray();
        }

        private static IEnumerable<string> GetUnderscorePathsFromAssetTypes(params Type[] assetTypes)
        {
            return assetTypes
                .SelectMany(type => type.Fields(Flags.StaticPublicDeclaredOnly))
                .Where(field => field.Name.StartsWith("_"))
                .Where(field => field.FieldType == typeof(string))
                .Where(field => field.IsLiteral)
                .Select(field => (string)field.GetRawConstantValue());
        }
    }
}
