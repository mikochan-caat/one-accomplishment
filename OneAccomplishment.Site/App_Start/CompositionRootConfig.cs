﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Web.Mvc;
using OneAccomplishment.Core.BusinessRules;
using OneAccomplishment.Core.Commands;
using OneAccomplishment.Core.Data;
using OneAccomplishment.Core.Decorators;
using OneAccomplishment.Core.Queries;
using OneAccomplishment.Core.Services;
using OneAccomplishment.Site.Core.Services;
using OneAccomplishment.Site.ViewModels.Validators;
using SimpleInjector;
using SimpleInjector.Integration.Web;
using SimpleInjector.Integration.Web.Mvc;
using CoreAssemblyMarker = OneAccomplishment.Core._AssemblyMarker;
using FakesAssemblyMarker = OneAccomplishment.Fakes._AssemblyMarker;
using SiteCoreAssemblyMarker = OneAccomplishment.Site.Core._AssemblyMarker;

namespace OneAccomplishment.Site
{
    public static partial class CompositionRootConfig
    {
        public static void Configure()
        {
            var thisAssembly = typeof(MvcApplication).Assembly;
            var coreAssembly = typeof(CoreAssemblyMarker).Assembly;
            var fakesAssembly = typeof(FakesAssemblyMarker).Assembly;
            var siteCoreAssembly = typeof(SiteCoreAssemblyMarker).Assembly;

            var container = new Container();
            container.Options.DefaultScopedLifestyle = new WebRequestLifestyle();

            // Register all MVC-specific classes
            container.RegisterMvcControllers(thisAssembly);
            container.RegisterSingleton<ITempDataProvider, TempDataProvider>();

            // Register all UI validators
            container.Register(typeof(AbstractUIValidator<>), thisAssembly, Lifestyle.Singleton);

            // Register factories and singleton services
            container.RegisterSingleton<IBusinessRulesService, BusinessRulesService>();
            container.RegisterSingleton<IQueryProcessor, QueryProcessor>();
            container.RegisterSingleton<IClientService, WebClientService>();
            container.RegisterSingleton<IDbCachingService, ObjectDataCachingService>();
            container.RegisterSingleton<ICachingService, ObjectDataCachingService>();
            container.Register<UnitOfWork>(
                () => UnitOfWork.Create(
                        container.GetInstance<ICachingService>(),
                        container.GetInstance<IDbCachingService>()), 
                Lifestyle.Scoped);

            // Register core services
            RegisterCoreServices(
                container, 
                siteCoreAssembly, 
                GetCoreServiceInterfaces(siteCoreAssembly), 
                Lifestyle.Scoped);
            RegisterCoreServices(container, coreAssembly, GetCoreServiceInterfaces(), Lifestyle.Scoped);

            // Register core business rules
            RegisterBusinessRules(container, coreAssembly, Lifestyle.Scoped);
            
            // Register command handlers
            container.Register(typeof(ICommandHandler<>), coreAssembly, Lifestyle.Scoped);

            // Register query handlers
            container.Register(typeof(IQueryHandler<,>), coreAssembly, Lifestyle.Scoped);

            // Register command handler decorators
            RegisterDecoratorChain(
                container,
                typeof(ICommandHandler<>),
                HandlerDecorators.GetCommandHandlerChain(),
                Lifestyle.Scoped);

            // Register query handler decorators
            RegisterDecoratorChain(
                container,
                typeof(IQueryHandler<,>),
                HandlerDecorators.GetQueryHandlerChain(),
                Lifestyle.Scoped);

            // Register fake implementations (if possible)
            container.Options.AllowOverridingRegistrations = true;
            ConfigureFakes(container, fakesAssembly);
            container.Options.AllowOverridingRegistrations = false;

            // Register unhandled types
            container.RegisterConditional(
                typeof(IRules<>),
                typeof(DefaultRules<>),
                Lifestyle.Scoped,
                context => !context.Handled);

            container.Verify();
            DependencyResolver.SetResolver(new SimpleInjectorDependencyResolver(container));
        }

        [Conditional("FAKES_ENABLED")]
        private static void ConfigureFakes(Container container, Assembly fakesAssembly)
        {
            // Register fake services
            RegisterCoreServices(container, fakesAssembly, GetCoreServiceInterfaces(), Lifestyle.Scoped);

            // Register fake business rules
            RegisterBusinessRules(container, fakesAssembly, Lifestyle.Scoped);

            // Register fake command handlers
            container.Register(typeof(ICommandHandler<>), fakesAssembly, Lifestyle.Scoped);

            // Register fake query handlers
            container.Register(typeof(IQueryHandler<,>), fakesAssembly, Lifestyle.Scoped);
        }

        private static void RegisterBusinessRules(
            Container container, Assembly sourceAssembly, Lifestyle lifestyle)
        {
            var ruleServiceTypes = container.GetTypesToRegister(typeof(AbstractRules<,>), sourceAssembly);
            foreach (var type in ruleServiceTypes) {
                var closedType = type.GetClosedTypeOf(typeof(IRules<>));
                container.Register(closedType, type, lifestyle);
            }
        }

        private static void RegisterCoreServices(
            Container container, Assembly sourceAssembly, IEnumerable<Type> serviceInterfaces,
            Lifestyle lifestyle)
        {
            foreach (var type in serviceInterfaces) {
                var serviceTypes = container.GetTypesToRegister(type, sourceAssembly);
                foreach (var typeToRegister in serviceTypes) {
                    container.Register(type, typeToRegister, lifestyle);
                }
            }
        }

        private static IEnumerable<Type> GetCoreServiceInterfaces(params Assembly[] additionalAssemblies)
        {
            var coreInterface = typeof(ICoreService);
            return (additionalAssemblies ?? new Assembly[0])
                .Union(new[] { coreInterface.Assembly })
                .SelectMany(asm => asm.GetTypes())
                .Where(t => t.IsInterface && coreInterface.IsAssignableFrom(t) && t != coreInterface);
        }

        private static void RegisterDecoratorChain(Container container, Type serviceType,
            IEnumerable<Type> decoratorTypes, Lifestyle lifestyle)
        {
            foreach (var type in decoratorTypes) {
                container.RegisterDecorator(serviceType, type, lifestyle);
            }
        }
    }
}
