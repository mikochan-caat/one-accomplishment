'use strict';

(() => {
    const modulesToLoad = [
        AMD.Vendor.TIPPY,
        AMD.Helpers.TIPPYHELPER
    ];
    define(AMD.Site.Shared.LAYOUT, modulesToLoad, onModuleLoad);

    function onModuleLoad(tippy: TippyJs.Tippy, tippyHelper: TippyHelper) {
        const ACTIVE_DROPDOWN_CLASS = 'clicked';

        return <SiteScriptModule>(() => {
            tippy('#user-dropdown', {
                content: tippyHelper(),
                onShow(instance) {
                    instance.reference.classList.add(ACTIVE_DROPDOWN_CLASS);
                },
                onHide(instance) {
                    instance.reference.classList.remove(ACTIVE_DROPDOWN_CLASS);
                }
            });
        }); 
    }
})();
