'use strict';

(() => {
    const modulesToLoad = [
        AMD.Vendor.KNOCKOUT,
        AMD.Helpers.FORMSVALIDATIONFACTORY,
        AMD.Helpers.HTMLJSONDATA,
    ];
    define(AMD.Site.Login.INDEX, modulesToLoad, onModuleLoad);

    type LoginModel = MvcModels.LoginViewModel;

    interface LoginViewModel {
        userName: KnockoutObservable<string>,
        password: KnockoutObservable<string>,
        isStayLoggedIn: KnockoutObservable<boolean>,
        isLoggingIn: KnockoutObservable<boolean>,
        canLogin: KnockoutComputed<boolean>,
        onFormSubmit(): boolean,
    }

    function onModuleLoad( 
        ko: KnockoutStatic,
        formsValidation: FormsValidationFactory,
        htmlJsonData: HtmlJsonData) {
        return <SiteScriptModule>(() => {
            const vmState = htmlJsonData.DataAttributeAsObject<LoginModel>(htmlJsonData.MainViewModel);
            if (vmState) {
                const vm = <LoginViewModel>{
                    userName: ko.observable(vmState.Username),
                    password: ko.observable(vmState.Password),
                    isStayLoggedIn: ko.observable(vmState.IsStayLoggedIn),
                    isLoggingIn: ko.observable(false),
                    onFormSubmit: function() {
                        this.isLoggingIn(true);
                        return true;
                    }
                };
                vm.canLogin = ko.computed(() => !!(vm.userName() && vm.password()) && !vm.isLoggingIn());
                ko.applyBindings(vm);
            } else {
                throw new Error('No view model state was found.');
            }
            formsValidation.bootstrap();
        }); 
    }
})();
