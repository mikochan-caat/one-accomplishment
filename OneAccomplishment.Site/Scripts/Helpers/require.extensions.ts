'use strict';

interface Require {
    module<T>(moduleToLoad: string): Promise<T>;
    modules(modulesToLoad: string[]): Promise<any[]>;
}

define(AMD.Helpers.REQUIRE_EXTENSIONS, () => {
    requirejs.module = (moduleToLoad) => {
        return new Promise((resolve, reject) => {
            require([moduleToLoad], (loadedModule: any) => resolve(loadedModule), (error: any) => reject(error));
        });
    };
    requirejs.modules = (modulesToLoad) => {
        return new Promise((resolve, reject) => {
            require(modulesToLoad, function() {
                resolve(Array.from(arguments));
            }, (error: any) => reject(error));
        });
    };
});
