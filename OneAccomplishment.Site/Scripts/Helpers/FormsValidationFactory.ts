'use strict';

type FormsValidationConfigurator = (validator: AspNetValidation.ValidationService) => void;
interface FormsValidationFactory {
    bootstrap(configurator?: FormsValidationConfigurator): void;
}

(() => {
    const modulesToLoad = [
        AMD.Vendor.ASPNET_VALIDATION,
    ];
    define(AMD.Helpers.FORMSVALIDATIONFACTORY, modulesToLoad, onModuleLoad);

    function onModuleLoad(aspnetValidation: AspNetValidation.Module) {
        class FormsValidationFactoryImpl implements FormsValidationFactory
        {
            bootstrap(configurator?: FormsValidationConfigurator) {
                var validator = new aspnetValidation.ValidationService();
                if (configurator) {
                    configurator(validator);
                }
                validator.bootstrap();
            }
        }

        return new FormsValidationFactoryImpl();
    }
})();
