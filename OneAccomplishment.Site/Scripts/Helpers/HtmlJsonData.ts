'use strict';

interface HtmlJsonData {
    readonly MainViewModel: string;
    DataAttributeAsObject<T>(dataId?: string): T | null;
}

(() => {
    define(AMD.Helpers.HTMLJSONDATA, onModuleLoad);

    function onModuleLoad() {
        class HtmlJsonDataImpl implements HtmlJsonData {
            readonly MainViewModel = 'main-vm';

            DataAttributeAsObject<T>(dataId?: string): T | null {
                let dataObject : T | null = null
                const dataAttributeExtra = dataId ? `-${dataId}` : '';
                const dataAttribute = `data-json${dataAttributeExtra}`;
                const metaDataElement = document.querySelector(`meta[${dataAttribute}]`);
                if (metaDataElement) {
                    const encodedJson = metaDataElement.getAttribute(dataAttribute);
                    if (encodedJson) {
                        dataObject = <T>JSON.parse(atob(encodedJson));
                    }
                }
                return dataObject;
            }
        }

        return new HtmlJsonDataImpl();
    }
})();
