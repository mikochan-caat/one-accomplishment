﻿using System.Web.Mvc;

namespace OneAccomplishment.Site.Areas
{
    /// <summary>
    /// This area registration is only here to fix the bug in MVC and T4MVC as specified here: 
    /// https://web.archive.org/web/20140913225339/https://mvccontrib.codeplex.com/workitem/7154
    /// https://github.com/T4MVC/T4MVC/issues/28
    /// </summary>
    public sealed class MainAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "Main";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "MainArea_Default",
                "Main/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
