﻿using System.Net;
using System.Web.Mvc;
using Fasterflect;
using OneAccomplishment.Core.Services;
using OneAccomplishment.Core.Services.Auth;

namespace OneAccomplishment.Site.Filters
{
    public sealed class PermissionsAuthorizeAttribute : AuthorizeAttribute
    {
        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            var actionDescriptor = filterContext.ActionDescriptor;
            var controllerDescriptor = actionDescriptor.ControllerDescriptor;

            var permissionFilter = actionDescriptor.Attribute<PermissionFilterAttribute>();
            if (permissionFilter == null) {
                permissionFilter = controllerDescriptor.Attribute<PermissionFilterAttribute>();
            }
            if (permissionFilter == null) {
                base.OnAuthorization(filterContext);
            } else {
                var principalService = DependencyResolver.Current.GetService<IPrincipalService>();
                if (!permissionFilter.Demand(principalService.CurrentPrincipal.Permissions)) {
                    this.HandleUnauthorizedRequest(filterContext);
                }
            }
        }

        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            if (!filterContext.IsChildAction) {
                filterContext.Result = new HttpStatusCodeResult(
                    HttpStatusCode.Forbidden,
                    "Permissions check failed.");
            } else {
                filterContext.Result = new EmptyResult();
            }
        }
    }
}
