﻿using System.Web.Mvc;
using System.Web.Routing;
using Fasterflect;
using OneAccomplishment.Site.Core.Mvc.Attributes;

namespace OneAccomplishment.Site.Filters
{
    public sealed class AuthorizeAccessAttribute : AuthorizeAttribute
    {
        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            base.HandleUnauthorizedRequest(filterContext);
            bool shouldInterceptRedirect = 
                !filterContext.HttpContext.Request.IsAjaxRequest() &&
                !filterContext.IsChildAction;
            if (shouldInterceptRedirect) {
                var actionDescriptor = filterContext.ActionDescriptor;
                bool markedAsNoReturnUrl =
                    actionDescriptor.ControllerDescriptor.HasAttribute<NoReturnUrlAttribute>() ||
                    actionDescriptor.HasAttribute<NoReturnUrlAttribute>();
                string returnUrl = null;
                if (!markedAsNoReturnUrl) {
                    returnUrl = filterContext.HttpContext.Request.RawUrl;
                }
                filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary {
                    { "action", MVC.Logout.ActionNames.Index },
                    { "controller", MVC.Logout.Name },
                    { "returnUrl", returnUrl }
                });
            }
        }
    }
}
