﻿using System;
using System.Security.Cryptography;
using System.Web;
using System.Web.Mvc;
using OneAccomplishment.Core.Services;
using OneAccomplishment.Site.Core.Extensions;

namespace OneAccomplishment.Site.Filters
{
    public sealed class CorrelateTempDataAttribute : ActionFilterAttribute
    {
        private static readonly Type TempDataCorrelationItemKey = TempDataCorrelationExtensions.TypeItemKey;

        private readonly IConfigurationService configService;
        private readonly string routeDataCorrelationKey;
        private readonly string tempDataCorrelationFormName;

        public CorrelateTempDataAttribute()
        {
            this.configService = DependencyResolver.Current.GetService<IConfigurationService>();
            this.routeDataCorrelationKey = this.configService.TempDataCorrelationParamName;
            this.tempDataCorrelationFormName = this.configService.TempDataCorrelationFormName;
        }

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (filterContext.IsChildAction) {
                base.OnActionExecuting(filterContext);
                return;
            }

            var httpItems = filterContext.HttpContext.Items;
            if (httpItems.Contains(TempDataCorrelationItemKey)) {
                throw new InvalidOperationException("The TempData correlation HTTP item is reserved.");
            }

            string correlationId;
            if (!this.TryGetCorrelationIdFromContext(filterContext.HttpContext, out correlationId)) {
                correlationId = this.CreateCorrelationId();
            }
            httpItems[TempDataCorrelationItemKey] = correlationId;
        }

        public override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            if (filterContext.IsChildAction) {
                base.OnActionExecuted(filterContext);
                return;
            }

            var httpItems = filterContext.HttpContext.Items;
            if (!filterContext.HttpContext.Request.IsAjaxRequest()) {
                var result = filterContext.Result as RedirectToRouteResult;
                if (result != null) {
                    string correlationId = httpItems[TempDataCorrelationItemKey] as string;
                    if (String.IsNullOrWhiteSpace(correlationId)) {
                        throw new InvalidOperationException(
                            "The TempData correlation HTTP item was not found or invalid.");
                    }
                    if (result.RouteValues.ContainsKey(this.routeDataCorrelationKey)) {
                        string errorMessage = String.Format(
                            "The route data key '{0}' is reserved.",
                            this.routeDataCorrelationKey);
                        throw new InvalidOperationException(errorMessage);
                    }
                    result.RouteValues.Add(this.routeDataCorrelationKey, correlationId);
                }
            } else {
                httpItems.Remove(TempDataCorrelationItemKey);
            }
        }

        private string CreateCorrelationId()
        {
            using (var sha = SHA512.Create()) {
                var hashedBytes = sha.ComputeHash(Guid.NewGuid().ToByteArray());
                return BitConverter.ToString(hashedBytes)
                    .Replace("-", String.Empty)
                    .ToLowerInvariant()
                    .Substring(0, 8);
            }
        }

        private bool TryGetCorrelationIdFromContext(HttpContextBase context, out string correlationId)
        {
            var request = context.Request;
            var sourcesToCheck = new Func<string>[] {
                () => request.Form[this.tempDataCorrelationFormName],
                () => request.QueryString[this.routeDataCorrelationKey]
            };
            foreach (var source in sourcesToCheck) {
                correlationId = source();
                if (!String.IsNullOrWhiteSpace(correlationId)) {
                    return true;
                }
            }
            correlationId = null;
            return false;
        }
    }
}
