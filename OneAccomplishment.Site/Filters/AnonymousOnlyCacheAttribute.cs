﻿using System;
using System.Web;
using System.Web.Mvc;
using Fasterflect;
using OneAccomplishment.Site.Core.Mvc.Attributes;

namespace OneAccomplishment.Site.Filters
{
    public sealed class AnonymousOnlyCacheAttribute : ActionFilterAttribute
    {
        public override void OnResultExecuting(ResultExecutingContext filterContext)
        {
            var httpContext = filterContext.HttpContext;
            if (!httpContext.Request.IsAjaxRequest() && !filterContext.IsChildAction) {
                var descriptor = new ReflectedControllerDescriptor(filterContext.Controller.GetType());
                var actionDescriptor = descriptor.FindAction(
                    filterContext.Controller.ControllerContext, 
                    filterContext.RequestContext.RouteData.GetRequiredString("action"));
                bool shouldDisableCache = 
                    !this.ControllerOrActionHasAttribute<AllowAnonymousAttribute>(actionDescriptor) ||
                    this.ControllerOrActionHasAttribute<DisableAnonymousCacheAttribute>(actionDescriptor);
                if (shouldDisableCache) {
                    var cachePolicy = filterContext.HttpContext.Response.Cache;
                    cachePolicy.SetCacheability(HttpCacheability.Private);
                    cachePolicy.SetRevalidation(HttpCacheRevalidation.AllCaches);
                    cachePolicy.SetMaxAge(TimeSpan.Zero);
                    cachePolicy.SetNoStore();
                }
            }
        }

        private bool ControllerOrActionHasAttribute<T>(ActionDescriptor action) where T : Attribute
        {
            return action.HasAttribute<T>() || action.ControllerDescriptor.HasAttribute<T>();
        }
    }
}
