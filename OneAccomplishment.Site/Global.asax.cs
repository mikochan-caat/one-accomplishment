﻿using System;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using JavaScriptEngineSwitcher.Core;
using Links;
using OneAccomplishment.Site.Bundling;
using OneAccomplishment.Site.Core.Services.Auth;

namespace OneAccomplishment.Site
{
    public class MvcApplication : HttpApplication
    {
        protected void Application_Start()
        {
            MvcHandler.DisableMvcResponseHeader = true;
            DbConfig.Configure();
            CompositionRootConfig.Configure();
            WebMarkupMinConfig.Configure();
            AreaRegistration.RegisterAllAreas();

            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            JsEngineSwitcherConfig.Configure(JsEngineSwitcher.Current);
            AMD.PreInitializeFiles(this.Context, SiteScripts.Core.require_config_generated_ts);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            AMD.Initialize(this.Context);
            UIValidationConfig.Configure();
        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {
            var httpAuthService = DependencyResolver.Current.GetService<IHttpAuthenticationService>();
            httpAuthService.CaptureAuthenticationCookie();
            AMD.GenerateRequireModulesScript(
                this.Context,
                BundleTable.Bundles,
                SiteScripts.Core.require_config_generated_ts);
        }
    }
}
