﻿using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Optimization;
using BundleTransformer.Core.Bundles;
using Links;
using Net40Utilities.Validation;
using OneAccomplishment.Site.Core.Utilities;

namespace OneAccomplishment.Site.Bundling
{
    /// <summary>
    /// This class manages all RequireJs AMD definitions for C#.
    /// <para>
    /// This class also provides runtime generators for interoperability
    /// with ASP.NET MVC bundling and RequireJs.
    /// </para>
    /// </summary>
    public static partial class AMD
    {
        private static readonly List<Module> ModulesList = new List<Module>();
        private static readonly Dictionary<string, Module> ModulesMap = new Dictionary<string, Module>(StringComparer.OrdinalIgnoreCase);
        private static readonly ThreadSafeInit Initializer = new ThreadSafeInit();
        private static readonly ThreadSafeInit Generator = new ThreadSafeInit();

        private static bool Initialized = false;
        private static bool Generated = false;

        public static void PreInitializeFiles(HttpContext context, params string[] files)
        {
            ArgumentGuard.For(() => context).IsNull().Throw();
            ArgumentGuard.For(() => files).IsNull().Throw();

            foreach (string preInitFile in files) {
                var fileInfo = new FileInfo(context.Server.MapPath(preInitFile));
                if (!fileInfo.Exists) {
                    fileInfo.CreateText().Dispose();
                }
            }
        }

        public static void Initialize(HttpContext context)
        {
            ArgumentGuard.For(() => context).IsNull().Throw();
            if (Initialized) { return; }

            bool performedInit = false;
            try {
                performedInit = Initializer.ExecuteThreadSafeOnce();
                if (Initialized) { return; }
                Initialized = true;

                foreach (var module in ModulesList) {
                    string modulePhysicalPath = context.Server.MapPath(module.PhysicalPath);
                    ModulesMap[modulePhysicalPath] = module;
                }
            } finally {
                if (performedInit) {
                    Initializer.EndThreadSafeOnceSection();
                }
            }
        }

        public static void GenerateRequireModulesScript(HttpContext context, BundleCollection bundles, string targetFile)
        {
            ArgumentGuard.For(() => context).IsNull().Throw();
            ArgumentGuard.For(() => bundles).IsNull().Throw();
            ArgumentGuard.For(() => targetFile).IsNull().Throw();
            if (Generated) { return; }

            bool performedInit = false;
            try {
                performedInit = Generator.ExecuteThreadSafeOnce();
                if (Generated) { return; }
                Generated = true;

                string templatePath = SiteScripts.Templates.require_config_template;
                string fileTemplate = File.ReadAllText(context.Server.MapPath(templatePath));

                var bundleResolver = new BundleResolver(bundles);
                var bundleFilesEnum = bundles
                    .OfType<CustomScriptBundle>()
                    .Select(b => new {
                        BundleUrl = bundleResolver.GetBundleUrl(b.Path),
                        Contents = bundleResolver.GetBundleContents(b.Path)
                    })
                    .SelectMany(b => b.Contents, (b, content) => new {
                        BundleUrl = b.BundleUrl,
                        FilePath = content,
                        Module = GetModuleFromBundlePath(context, content)
                    })
                    .Where(b => b.Module != null)
                    .GroupBy(b => b.BundleUrl);

                using (var pathsWriter = new StringWriter(new StringBuilder(2048)))
                using (var bundlesWriter = new StringWriter(new StringBuilder(2048)))
                using (var pathsCodeWriter = new IndentedTextWriter(pathsWriter))
                using (var bundlesCodeWriter = new IndentedTextWriter(bundlesWriter)) {
                    pathsCodeWriter.Write("paths: {");
                    pathsCodeWriter.Indent += 2;
                    bundlesCodeWriter.Write("bundles: {");
                    bundlesCodeWriter.Indent += 2;

                    foreach (var bundle in bundleFilesEnum) {
                        if (!context.IsDebuggingEnabled) {
                            bundlesCodeWriter.WriteLine();
                            bundlesCodeWriter.Write("'{0}': [", bundle.Key);
                            bundlesCodeWriter.Indent++;
                        }

                        foreach (var bundledFile in bundle) {
                            var module = bundledFile.Module;
                            pathsCodeWriter.WriteLine();
                            pathsCodeWriter.Write("'{0}': '{1}',", module.ModulePath, module.VirtualPath);

                            if (!context.IsDebuggingEnabled) {
                                bundlesCodeWriter.WriteLine();
                                bundlesCodeWriter.Write("'{0}',", module.ModulePath);
                            }
                        }

                        if (!context.IsDebuggingEnabled) {
                            bundlesCodeWriter.Indent--;
                            bundlesCodeWriter.WriteLine();
                            bundlesCodeWriter.Write("],", bundle.Key);
                        }
                    }

                    pathsCodeWriter.Indent--;
                    pathsCodeWriter.WriteLine();
                    pathsCodeWriter.Write("},");
                    bundlesCodeWriter.Indent--;
                    bundlesCodeWriter.WriteLine();
                    bundlesCodeWriter.Write("},");

                    using (var output = File.Open(context.Server.MapPath(targetFile), FileMode.Create))
                    using (var outputWriter = new StreamWriter(output, Encoding.UTF8)) {
                        string pathsContent = pathsWriter.ToString();
                        string bundlesContent = context.IsDebuggingEnabled ? null : bundlesWriter.ToString();
                        string fileContents = String.Format(fileTemplate, pathsContent, bundlesContent);

                        outputWriter.WriteLine(fileContents);
                    }
                }
            } finally {
                if (performedInit) {
                    Generator.EndThreadSafeOnceSection();
                }
            }
        }

        private static Module GetModuleFromBundlePath(HttpContext context, string bundlePath)
        {
            string bundlePhysicalPath = context.Server.MapPath(bundlePath);
            Module module;
            ModulesMap.TryGetValue(bundlePhysicalPath, out module);
            return module;
        }

        private sealed class Module
        {
            public string PhysicalPath { get; set; }
            public string VirtualPath { get; set; }
            public string ModulePath { get; set; }
        }
    }
}
