﻿namespace OneAccomplishment.Site.Bundling
{
    public static class Bundles
    {
        public static class Core
        {
            public const string Scripts = "~/bundles/core/scripts";
        }

        public static class Site
        {
            public const string Modules = "~/bundles/site/modules";
            public const string Styles = "~/bundles/site/styles";
            public const string FallbackStyles = "~/bundles/site/fallback-styles";
        }

        public static class Vendor
        {
            public const string Modules = "~/bundles/vendor/modules";
            public const string Styles = "~/bundles/vendor/styles";
        }
    }
}
