﻿using System.Web;

namespace OneAccomplishment.Site
{
    public sealed class ETagHttpModule : IHttpModule
    {
        public void Init(HttpApplication context)
        {
            context.PostReleaseRequestState += (sender, args) => {
                var httpContext = HttpContext.Current;
                if (!httpContext.IsDebuggingEnabled) {
                    httpContext.Response.Headers.Remove("ETag");
                }
            };
        }

        public void Dispose()
        {
        }
    }
}
