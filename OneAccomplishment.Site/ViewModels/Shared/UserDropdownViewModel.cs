﻿namespace OneAccomplishment.Site.ViewModels.Shared
{
    public sealed class UserDropdownViewModel : IViewModel
    {
        public string Username { get; set; }
        public string Initials { get; set; }
        public string DisplayName { get; set; }
        public string EmailAddress { get; set; }

        public string FirstRoleName { get; set; }
        public int RoleCount { get; set; }
        public int AdditionalRoleCount { get; set; }
        public bool HasMoreThanOneRole { get; set; }
    }
}
