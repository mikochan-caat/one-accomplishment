﻿using System.Linq;
using System.Reflection;
using Reinforced.Typings.Fluent;

namespace OneAccomplishment.Site.ViewModels
{
    public static class TypescriptModelGenerator
    {
        public static void Configure(ConfigurationBuilder builder)
        {
            builder.Global(config => {
                config.TabSymbol(new string(' ', 4))
                    .UseModules(false, false)
                    .ExportPureTypings();
            });
            var baseType = typeof(IViewModel);
            var modelTypes = typeof(MvcApplication).Assembly
                .GetExportedTypes()
                .Where(t => t.IsClass && !t.IsAbstract && baseType.IsAssignableFrom(t));
            builder.ExportAsClasses(modelTypes, config => {
                config
                    .WithProperties(BindingFlags.Public | BindingFlags.Instance, propConfig => {
                        propConfig.ForceNullable();
                    })
                    .OverrideNamespace("MvcModels");
            });
        }
    }
}
