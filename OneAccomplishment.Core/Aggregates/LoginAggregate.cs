﻿using Net40Utilities.Validation;
using OneAccomplishment.Core.BusinessRules;
using OneAccomplishment.Core.BusinessRules.Violations;
using OneAccomplishment.Core.Commands;
using OneAccomplishment.Core.Commands.Logins;
using OneAccomplishment.Core.Models;
using OneAccomplishment.Core.Services;

namespace OneAccomplishment.Core.Aggregates
{
    public sealed class LoginAggregate : ICoreService
    {
        private readonly ICommandHandler<AddLoginDataCommand> addLoginDataHandler;
        private readonly ICommandHandler<LoginAttemptCommand> loginAttemptHandler;
        private readonly ICommandHandler<FailedLoginDataCommand> failedLoginAttemptHandler;

        public LoginAggregate(
            ICommandHandler<AddLoginDataCommand> addLoginDataHandler,
            ICommandHandler<LoginAttemptCommand> loginAttemptHandler,
            ICommandHandler<FailedLoginDataCommand> failedLoginAttemptHandler)
        {
            ArgumentGuard.For(() => addLoginDataHandler).IsNull().Throw();
            ArgumentGuard.For(() => loginAttemptHandler).IsNull().Throw();
            ArgumentGuard.For(() => failedLoginAttemptHandler).IsNull().Throw();

            this.addLoginDataHandler = addLoginDataHandler;
            this.loginAttemptHandler = loginAttemptHandler;
            this.failedLoginAttemptHandler = failedLoginAttemptHandler;
        }

        public void Authenticate(Login login)
        {
            ArgumentGuard.For(() => login).IsNull().Throw();

            this.addLoginDataHandler.Handle(new AddLoginDataCommand {
                Username = login.Username
            });
            try {
                this.loginAttemptHandler.Handle(new LoginAttemptCommand {
                    Username = login.Username,
                    Password = login.Password,
                    IsStayLoggedIn = login.IsStayLoggedIn
                });
            } catch (BusinessRuleViolationException<LoginViolations> ex) {
                if (ex.ViolatedRule != LoginViolations.LoginAttemptExceeded) {
                    this.failedLoginAttemptHandler.Handle(new FailedLoginDataCommand {
                        Username = login.Username
                    });
                }
                throw;
            }
        }
    }
}
