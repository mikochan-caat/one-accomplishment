﻿using OneAccomplishment.Core.Services.Auth;

namespace OneAccomplishment.Core.Services
{
    /// <summary>
    /// Provides an abstract API for user details, capabilities, and permissions.
    /// </summary>
    public interface IPrincipalService : ICoreService
    {
        /// <summary>
        /// Gets the current user's principal information.
        /// </summary>
        IUserPrincipal CurrentPrincipal { get; }
    }
}
