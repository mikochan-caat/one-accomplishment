﻿using OneAccomplishment.Core.BusinessRules;
using OneAccomplishment.Core.Commands;

namespace OneAccomplishment.Core.Services
{
    /// <summary>
    /// Provides an abstract API for business rules validators.
    /// </summary>
    public interface IBusinessRulesService
    {
        /// <summary>
        /// Gets the business rules validator for the specified type.
        /// </summary>
        /// <typeparam name="T">Command/Query/Object type.</typeparam>
        /// <returns>The business rules validator for the specified type.</returns>
        IRules<T> GetRulesFor<T>() where T : ICommand;
    }
}
