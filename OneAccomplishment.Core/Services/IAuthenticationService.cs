﻿using OneAccomplishment.Core.Services.Auth;

namespace OneAccomplishment.Core.Services
{
    /// <summary>
    /// Provides an abstract authentication API.
    /// </summary>
    public interface IAuthenticationService : ICoreService
    {
        /// <summary>
        /// Occurs when an authentication ticket is revoked.
        /// <para>All attached handler lifetimes are automatically managed.</para>
        /// </summary>
        event AuthenticationRevokedEventHandler OnAuthenticationRevoked;

        /// <summary>
        /// The current authentication request based on the current
        /// request scope.
        /// </summary>
        IExpiringAuthenticationTicket CurrentTicket { get; }

        /// <summary>
        /// Grants the provided authentication request and 
        /// marks the user as known.
        /// </summary>
        /// <param name="authTicket">The request to grant.</param>
        void GrantAuthentication(IAuthenticationTicket authTicket);
        /// <summary>
        /// Revokes the provided authentication request and 
        /// marks the user as anonymous.
        /// </summary>
        /// <param name="authTicket">The request to revoke.</param>
        void RevokeAuthentication(IAuthenticationTicket authTicket);
    }

    /// <summary>
    /// A callback indicating that an authentication ticket was revoked.
    /// </summary>
    /// <param name="revokedTicket">The revoked ticket.</param>
    public delegate void AuthenticationRevokedEventHandler(IAuthenticationTicket revokedTicket);
}
