﻿using System;

namespace OneAccomplishment.Core.Services.Configuration
{
    public sealed class SystemConfigurationException : Exception
    {
        public SystemConfigurationException()
            : base("System configuration was not found or set.")
        {
        }

        public SystemConfigurationException(string configName)
            : base(String.Format("A null system configuration value was found for '{0}'.", configName))
        {
        }
    }
}
