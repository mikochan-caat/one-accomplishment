﻿namespace OneAccomplishment.Core.Services.Auth
{
    /// <summary>
    /// Represents the available PermissionFilter operations.
    /// </summary>
    public enum PermissionOperation
    {
        /// <summary>All listed permissions must be available.</summary>
        And,
        /// <summary>Any of the listed permissions must be available.</summary>
        Or
    }
}
