﻿namespace OneAccomplishment.Core.Services.Auth
{
    /// <summary>
    /// Represents an authentication request.
    /// </summary>
    public interface IAuthenticationTicket
    {
        /// <summary>
        /// The domain ID to authenticate.
        /// </summary>
        string Username { get; }
        /// <summary>
        /// The internal ID of the domain ID.
        /// </summary>
        long LoginRefId { get; }
        /// <summary>
        /// Determines if this request is long-lived.
        /// </summary>
        bool IsStayLoggedIn { get; }
        /// <summary>
        /// Determines if this authentication request is valid.
        /// </summary>
        bool IsValidTicket { get; }
    }
}
