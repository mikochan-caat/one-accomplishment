﻿using System;
using System.Collections;
using System.Collections.Generic;
using Net40Utilities.Validation;

namespace OneAccomplishment.Core.Services.Auth
{
    /// <summary>
    /// Represents a collection of Permission IDs.
    /// </summary>
    public sealed class PermissionSet : IEnumerable<long>
    {
        private readonly HashSet<long> permissionIds;

        /// <summary>Gets the unique identifier of this PermissionSet.</summary>
        /// <value>The set unique identifier.</value>
        public Guid SetGuid { get; private set; }

        /// <summary>Initializes a new instance of the <see cref="PermissionSet"/> class.</summary>
        /// <param name="permissionIds">A predefined collection of Permission IDs.</param>
        public PermissionSet(IEnumerable<long> permissionIds)
        {
            ArgumentGuard.For(() => permissionIds).IsNull().Throw();

            this.permissionIds = new HashSet<long>(permissionIds);
            this.SetGuid = Guid.NewGuid();
        }

        /// <summary>Determines whether this set contains the specified permission.</summary>
        /// <param name="permissionId">The Permission ID to check.</param>
        /// <returns><c>true</c> if this set contains the specified permission; otherwise, <c>false</c>.</returns>
        public bool HasPermission(long permissionId)
        {
            return this.permissionIds.Contains(permissionId);
        }

        /// <summary>Determines whether this set contains any of the specified permissions.</summary>
        /// <param name="permissions">The collection of permissions to compare against.</param>
        /// <returns><c>true</c> if this set contains any of the specified permissions; otherwise, <c>false</c>.</returns>
        public bool HasAnyPermissions(params long[] permissions)
        {
            ArgumentGuard.For(() => permissions).IsNull().Throw();

            return this.HasAnyPermissions((IEnumerable<long>)permissions);
        }

        /// <summary>Determines whether this set contains all of the specified permissions.</summary>
        /// <param name="permissions">The collection of permissions to compare against.</param>
        /// <returns><c>true</c> if this set contains all of the specified permissions; otherwise, <c>false</c>.</returns>
        public bool HasAllPermissions(params long[] permissions)
        {
            ArgumentGuard.For(() => permissions).IsNull().Throw();

            return this.HasAllPermissions((IEnumerable<long>)permissions);
        }

        /// <summary>Determines whether this set contains any permission in the specified PermissionSet.</summary>
        /// <param name="permissions">The other PermissionSet to compare against.</param>
        /// <returns><c>true</c> if this set contains any permission in the specified PermissionSet; otherwise, <c>false</c>.</returns>
        public bool HasAnyPermissions(PermissionSet permissions)
        {
            ArgumentGuard.For(() => permissions).IsNull().Throw();

            return this.HasAnyPermissions(permissions.permissionIds);
        }

        /// <summary>Determines whether this set contains all permissions in the specified PermissionSet.</summary>
        /// <param name="permissions">The other PermissionSet to compare against.</param>
        /// <returns><c>true</c> if this set contains all permissions in the specified PermissionSet; otherwise, <c>false</c>.</returns>
        public bool HasAllPermissions(PermissionSet permissions)
        {
            ArgumentGuard.For(() => permissions).IsNull().Throw();

            return this.HasAllPermissions(permissions.permissionIds);
        }

        /// <summary>Determines whether this set contains any of the specified permissions.</summary>
        /// <param name="permissions">The collection of permissions to compare against.</param>
        /// <returns><c>true</c> if this set contains any of the specified permissions; otherwise, <c>false</c>.</returns>
        public bool HasAnyPermissions(IEnumerable<long> permissions)
        {
            ArgumentGuard.For(() => permissions).IsNull().Throw();

            return this.permissionIds.Overlaps(permissions);
        }

        /// <summary>Determines whether this set contains all of the specified permissions.</summary>
        /// <param name="permissions">The collection of permissions to compare against.</param>
        /// <returns><c>true</c> if this set contains all of the specified permissions; otherwise, <c>false</c>.</returns>
        public bool HasAllPermissions(IEnumerable<long> permissions)
        {
            ArgumentGuard.For(() => permissions).IsNull().Throw();

            return this.permissionIds.IsSupersetOf(permissions);
        }

        /// <summary>Returns an enumerator that iterates through the collection.</summary>
        /// <returns>
        /// A <see cref="T:System.Collections.Generic.IEnumerator`1" /> that can be used to iterate through the collection.
        /// </returns>
        public IEnumerator<long> GetEnumerator()
        {
            return this.permissionIds.GetEnumerator();
        }

        /// <summary>Returns an enumerator that iterates through a collection.</summary>
        /// <returns>An <see cref="T:System.Collections.IEnumerator" /> object that can be used to iterate through the collection.</returns>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }
    }
}
