﻿namespace OneAccomplishment.Core.Services.Auth
{
    /// <summary>
    /// Represents an authentication request with expiration information.
    /// </summary>
    public interface IExpiringAuthenticationTicket: IAuthenticationTicket
    {
        /// <summary>
        /// Determines if this authentication request has expired.
        /// </summary>
        bool IsExpired { get; }
    }
}
