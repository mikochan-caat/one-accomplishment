﻿using System;
using OneAccomplishment.Core.Services.Caching;

namespace OneAccomplishment.Core.Services
{
    /// <summary>
    /// Provides an abstract global caching API.
    /// </summary>
    public interface ICachingService
    {
        /// <summary>
        /// Gets the current object stored with the specified key.
        /// </summary>
        /// <typeparam name="T">Object type.</typeparam>
        /// <param name="key">Cache key to use.</param>
        /// <param name="options">Additional cache retrieval options.</param>
        /// <returns>The object stored in the specified key.</returns>
        T Get<T>(string key, CacheOptions options = null);
        /// <summary>
        /// Gets the current object stored with the specified key or creates 
        /// it using the specified factory function if it does not.
        /// </summary>
        /// <typeparam name="T">Object type.</typeparam>
        /// <param name="key">Cache key to use.</param>
        /// <param name="factory">The object generator if it does exist.</param>
        /// <param name="options">Additional cache retrieval options.</param>
        /// <returns>The object stored in the specified key.</returns>
        T GetOrAdd<T>(string key, Func<T> factory, CacheOptions options = null);
        /// <summary>
        /// Adds an object with the specified key.
        /// </summary>
        /// <typeparam name="T">Object type.</typeparam>
        /// <param name="key">Cache key to use.</param>
        /// <param name="item">The object to add.</param>
        /// <param name="options">Additional cache setting options.</param>
        void Add<T>(string key, T item, CacheOptions options = null);
        /// <summary>
        /// Removes an object with the specified key.
        /// </summary>
        /// <param name="key">Cache key to use.</param>
        /// <param name="options">Additional cache removal options.</param>
        void Remove(string key, CacheOptions options = null);
    }
}
