﻿namespace OneAccomplishment.Core.Services.Caching
{
    /// <summary>
    /// Represents the type of expiration for a cache entry.
    /// </summary>
    public enum CacheExpiry
    {
        /// <summary>The cache entry will expire at a point in time.</summary>
        Absolute,
        /// <summary>The cache entry will expire if not accessed within a period of time.</summary>
        Sliding
    }
}
