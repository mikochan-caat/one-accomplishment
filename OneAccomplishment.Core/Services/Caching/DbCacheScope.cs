﻿namespace OneAccomplishment.Core.Services.Caching
{
    /// <summary>
    /// Represents the locality of a query cache entry.
    /// </summary>
    public enum DbCacheScope
    {
        /// <summary>The cache entry is scoped to the whole application.</summary>
        Global,
        /// <summary>The cache entry is scoped to the current scope/request.</summary>
        Current
    }
}
