﻿using System;
using System.Collections.Generic;
using System.Linq;
using LinqToDB;
using Net40Utilities.Validation;
using OneAccomplishment.Core.Services;
using OneAccomplishment.Core.Services.Caching;

namespace OneAccomplishment.Core.Data
{
    public sealed partial class UnitOfWork : ICachingService, IDbCachingService
    {
        private static readonly string LinqToDbAssemblyName = typeof(LinqExtensions).Assembly.FullName;

        private readonly ICachingService cacheService;
        private readonly IDbCachingService dbCachingService;

        private UnitOfWork(ICachingService cacheService, IDbCachingService dataCacheService)
            : base((string)null)
        {
            this.cacheService = cacheService;
            this.dbCachingService = dataCacheService;
        }

        T ICachingService.Get<T>(string key, CacheOptions options)
        {
            return this.cacheService.Get<T>(key, options);
        }

        T ICachingService.GetOrAdd<T>(string key, Func<T> factory, CacheOptions options)
        {
            return this.cacheService.GetOrAdd(key, factory, options);
        }

        void ICachingService.Add<T>(string key, T item, CacheOptions options)
        {
            this.cacheService.Add(key, item, options);
        }

        void ICachingService.Remove(string key, CacheOptions options)
        {
            this.cacheService.Remove(key, options);
        }

        IEnumerable<T> IDbCachingService.FromCache<T>(
            IQueryable<T> query, DbCacheScope scope, DbCacheOptions options)
        {
            this.ValidateQuery(query);
            return this.dbCachingService.FromCache(query, scope, options);
        }

        private void ValidateQuery<T>(IQueryable<T> query)
        {
            ArgumentGuard.For(() => query).IsNull().Throw();

            var queryType = query.GetType();
            var queryAssemblyName = queryType.Assembly.FullName;
            if (!LinqToDbAssemblyName.Equals(queryAssemblyName, StringComparison.OrdinalIgnoreCase)) {
                string errorMessage = String.Format("Query type '{0}' is not supported.", queryType.FullName);
                throw new NotSupportedException(errorMessage);
            }
        }

        public static UnitOfWork Create(ICachingService cacheService, IDbCachingService dataCacheService)
        {
            ArgumentGuard.For(() => cacheService).IsNull().Throw();
            ArgumentGuard.For(() => dataCacheService).IsNull().Throw();

            return new UnitOfWork(cacheService, dataCacheService);
        }
    }
}
