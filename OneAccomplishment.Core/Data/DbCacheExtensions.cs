﻿using System.Collections.Generic;
using System.Linq;
using Net40Utilities.Validation;
using OneAccomplishment.Core.Services;
using OneAccomplishment.Core.Services.Caching;

namespace OneAccomplishment.Core.Data
{
    internal static class DbCacheExtensions
    {
        public static IEnumerable<T> FromCache<T>(
            this IQueryable<T> query, UnitOfWork unitOfWork, DbCacheScope scope, DbCacheOptions options = null)
        {
            ArgumentGuard.For(() => query).IsNull().Throw();
            ArgumentGuard.For(() => unitOfWork).IsNull().Throw();

            return ((IDbCachingService)unitOfWork).FromCache(query, scope, options);
        }
    }
}
