﻿namespace OneAccomplishment.Core.Commands
{
    /// <summary>
    /// Provides the implementation of a request from a command.
    /// </summary>
    /// <typeparam name="TCommand">Command type to handle.</typeparam>
    public interface ICommandHandler<TCommand> where TCommand : class, ICommand
    {
        /// <summary>
        /// Executes the provided command.
        /// </summary>
        /// <param name="command">Command to handle.</param>
        void Handle(TCommand command);
    }
}
