﻿using System.Collections.Generic;
using FluentValidation;
using Net40Utilities.Validation;
using OneAccomplishment.Core.BusinessRules;
using OneAccomplishment.Core.BusinessRules.Violations;
using OneAccomplishment.Core.Services;

namespace OneAccomplishment.Core.Commands.Logins
{
    internal sealed class AddLoginDataCommandRules : AbstractLoginCommandRules<AddLoginDataCommand>
    {
        private readonly IDomainService domainService;

        public AddLoginDataCommandRules(IDomainService domainService)
        {
            ArgumentGuard.For(() => domainService).IsNull().Throw();

            this.domainService = domainService;
        }

        protected override IEnumerable<RuleViolationDelegate> GetRuleChainLinks()
        {
            yield return this.Username_Must_Not_Be_Empty();
            yield return this.Username_Must_Exist_In_Domain();
        }

        private RuleViolationDelegate Username_Must_Not_Be_Empty()
        {
            return validator => validator.RuleFor(cmd => cmd.Username)
                .NotEmpty()
                .WithViolation(LoginViolations.MissingRequiredValue);
        }

        private RuleViolationDelegate Username_Must_Exist_In_Domain()
        {
            return validator => validator.RuleFor(cmd => cmd)
                .Must(cmd => this.domainService.IsUsernameExisting(cmd.Username))
                .WithViolation(LoginViolations.InvalidCredentials);
        }
    }
}
