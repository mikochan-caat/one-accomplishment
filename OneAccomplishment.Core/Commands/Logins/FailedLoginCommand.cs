﻿using OneAccomplishment.Core.Decorators.Config;
using OneAccomplishment.Core.Decorators.Traits;

namespace OneAccomplishment.Core.Commands.Logins
{
    [AuditTrailOptions("Failed login attempt.")]
    public sealed class FailedLoginDataCommand : ICommand, IAuditable
    {
        public string Username { get; set; }
    }
}
