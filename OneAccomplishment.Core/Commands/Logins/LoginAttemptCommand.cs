﻿using OneAccomplishment.Core.Decorators.Config;
using OneAccomplishment.Core.Decorators.Traits;

namespace OneAccomplishment.Core.Commands.Logins
{
    [AuditTrailOptions("User successfully logged in.")]
    public sealed class LoginAttemptCommand : ICommand, IAuditable
    {
        public string Username { get; set; }
        [AuditTrailSensitiveData]
        public string Password { get; set; }
        public bool IsStayLoggedIn { get; set; }
    }
}
