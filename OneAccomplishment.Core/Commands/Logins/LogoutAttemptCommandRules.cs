﻿using System.Collections.Generic;
using FluentValidation;
using OneAccomplishment.Core.BusinessRules;
using OneAccomplishment.Core.BusinessRules.Violations;

namespace OneAccomplishment.Core.Commands.Logins
{
    internal sealed class LogoutAttemptCommandRules : AbstractLoginCommandRules<LogoutAttemptCommand>
    {
        protected override IEnumerable<RuleViolationDelegate> GetRuleChainLinks()
        {
            yield return this.Username_Must_Not_Be_Empty();
            yield return this.Authentication_Ticket_Must_Not_Be_Null();
            yield return this.Current_Ticket_Is_Valid();
        }

        private RuleViolationDelegate Username_Must_Not_Be_Empty()
        {
            return validator => validator.RuleFor(cmd => cmd.Reason)
                .NotEmpty()
                .WithViolation(LoginViolations.MissingRequiredValue);
        }

        private RuleViolationDelegate Authentication_Ticket_Must_Not_Be_Null()
        {
            return validator => validator.RuleFor(cmd => cmd.AuthenticationTicket)
                .NotNull()
                .WithViolation(LoginViolations.MissingRequiredValue);
        }

        private RuleViolationDelegate Current_Ticket_Is_Valid()
        {
            return validator => validator.RuleFor(cmd => cmd.AuthenticationTicket)
                .Must(ticket => ticket.IsValidTicket)
                .WithViolation(LoginViolations.CurrentAuthenticationInvalid);
        }
    }
}
