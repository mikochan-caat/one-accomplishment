﻿using System.Collections.Generic;
using FluentValidation;
using OneAccomplishment.Core.BusinessRules;
using OneAccomplishment.Core.BusinessRules.Violations;

namespace OneAccomplishment.Core.Commands.Logins
{
    internal sealed class FailedLoginDataCommandRules : AbstractLoginCommandRules<FailedLoginDataCommand>
    {
        protected override IEnumerable<RuleViolationDelegate> GetRuleChainLinks()
        {
            yield return this.Username_Must_Not_Be_Empty();
        }

        private RuleViolationDelegate Username_Must_Not_Be_Empty()
        {
            return validator => validator.RuleFor(cmd => cmd.Username)
                .NotEmpty()
                .WithViolation(LoginViolations.MissingRequiredValue);
        }
    }
}
