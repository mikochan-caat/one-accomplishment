﻿using FluentValidation;

namespace OneAccomplishment.Core.BusinessRules
{
    internal delegate IRuleBuilderOptions<T, object>
        RuleChainLinkDelegate<T>(AbstractValidator<T> validator);
}
