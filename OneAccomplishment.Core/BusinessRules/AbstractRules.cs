﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using FluentValidation;
using OneAccomplishment.Core.Commands;

namespace OneAccomplishment.Core.BusinessRules
{
    /// <summary>
    /// The base class for specialized business rule validators.
    /// </summary>
    /// <typeparam name="T">The command type to validate.</typeparam>
    /// <typeparam name="TViolation">The type of rule violation for this validator.</typeparam>
    public abstract class AbstractRules<T, TViolation> : IRules<T> 
        where T : ICommand
        where TViolation : IRuleViolation<TViolation>
    {
        protected internal delegate IRuleViolationBuilderOptions<T, object, TViolation>
            RuleViolationDelegate(AbstractValidator<T> validator);

        private readonly InlineValidator<T> validator;
        private readonly Lazy<bool> lazyRuleChainInitializer;

        public AbstractRules()
        {
            this.validator = new InlineValidator<T>();
            this.lazyRuleChainInitializer = new Lazy<bool>(
                this.RuleChainInitializer,
                LazyThreadSafetyMode.ExecutionAndPublication);
        }

        /// <summary>
        /// Validates a command with a defined set of business rules.
        /// <para>If validation fails, this method will throw a BusinessRuleViolationException of the proper type.</para>
        /// </summary>
        /// <param name="command">The command to validate.</param>
        /// <exception cref="System.InvalidOperationException">
        /// A business rule validator has more than 1 validation error.
        /// <para>A business rule validator does not have a rule violation.</para>
        /// </exception>
        /// <exception cref="BusinessRuleViolationException{TViolation}">
        /// If validation of business rules fails.
        /// </exception>
        public void ValidateCommand(T command)
        {
            if (!this.lazyRuleChainInitializer.Value) {
                // This will never happen, we just need to initialize.
                return;
            }

            var validationResults = this.validator.Validate(command);
            if (!validationResults.IsValid) {
                var errors = validationResults.Errors;
                if (errors.Count > 1) {
                    throw new InvalidOperationException(
                        "A business rule validator must have only 1 validation error. " +
                        "See the InnerException property for validation error details.",
                        new ValidationException(errors));
                }

                var validationFailure = errors.First();
                TViolation violatedRule;
                try {
                    violatedRule = (TViolation)validationFailure.CustomState;
                } catch (InvalidCastException) {
                    throw new InvalidOperationException(
                        "A business rule validator must have a rule violation. " +
                        "See the InnerException property for validation error details.",
                        new ValidationException(errors));
                }

                throw new BusinessRuleViolationException<TViolation>(
                    this.GetType(), command, violatedRule, validationFailure); 
            }
        }

        protected abstract IEnumerable<RuleViolationDelegate> GetRuleChainLinks();

        private bool RuleChainInitializer()
        {
            this.BuildRuleChain();
            return true;
        }

        private void BuildRuleChain()
        {
            var chainLinks = this.GetRuleChainLinks();
            if (chainLinks == null) {
                throw new InvalidOperationException("This rule must contain rule chain links.");
            }

            var chainedRules = chainLinks
                .Select(link => (RuleChainLinkDelegate<T>)(validator => link(validator).AssociatedRule))
                .ToArray();
            this.validator.ChainRules(chainedRules);
        }
    }
}
