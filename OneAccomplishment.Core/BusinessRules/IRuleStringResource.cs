﻿using System;

namespace OneAccomplishment.Core.BusinessRules
{
    /// <summary>
    /// Provides a key-value pair for rule string resources.
    /// </summary>
    /// <typeparam name="T">The implementing type of this interface.</typeparam>
    /// <seealso cref="System.IEquatable{T}" />
    public interface IRuleStringResource<T> : IEquatable<T> where T : IRuleStringResource<T>
    {
        /// <summary>Gets the key of this rule string resource.</summary>
        /// <value>The key.</value>
        string Key { get; }
        /// <summary>Gets the value of this rule string resource (i.e. the content).</summary>
        /// <value>The value.</value>
        string Value { get; }
    }
}
