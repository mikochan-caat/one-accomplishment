﻿namespace OneAccomplishment.Core.BusinessRules
{
    /// <summary>Provides rule string resource values for rule violations.</summary>
    /// <typeparam name="T">The implementing type of this interface.</typeparam>
    /// <seealso cref="OneAccomplishment.Core.BusinessRules.IRuleStringResource{T}" />
    public interface IRuleViolation<T> : IRuleStringResource<T> where T : IRuleViolation<T>
    {
        /// <summary>Gets the associated message for this rule violation.</summary>
        /// <value>The associated message.</value>
        string AssociatedMessage { get; }
    }
}
