﻿using OneAccomplishment.Core.BusinessRules.Messages;

namespace OneAccomplishment.Core.BusinessRules.Violations
{
    public partial class LoginViolations
    {
        public override string AssociatedMessage
        {
            get { return LoginMessages.ResolveKey(this.key); }
        }
    }
}
