﻿using FluentValidation;
using OneAccomplishment.Core.Commands;

namespace OneAccomplishment.Core.BusinessRules
{
    /// <summary>Provides configuration options for a rule violation.</summary>
    /// <typeparam name="T">The type of the validated command.</typeparam>
    /// <typeparam name="TProperty">The type of a property from the validated command.</typeparam>
    /// <typeparam name="TViolation">The type of rule violation.</typeparam>
    public interface IRuleViolationBuilderOptions<T, out TProperty, TViolation>
        where T : ICommand
        where TViolation : IRuleViolation<TViolation>
    {
        /// <summary>Gets the associated rule for this violation.</summary>
        /// <value>The associated rule.</value>
        IRuleBuilderOptions<T, TProperty> AssociatedRule { get; }
        /// <summary>Gets the violated rule.</summary>
        /// <value>The violated rule.</value>
        TViolation ViolatedRule { get; }
    }
}
