﻿namespace OneAccomplishment.Core.Queries
{
    /// <summary>
    /// Provides the implementation of a request from a query.
    /// </summary>
    /// <typeparam name="TQuery">Query type to handle.</typeparam>
    /// <typeparam name="TResult">Query return type.</typeparam>
    public interface IQueryHandler<TQuery, TResult> where TQuery : class, IQuery<TResult>
    {
        /// <summary>
        /// Executes the provided query.
        /// </summary>
        /// <param name="query">Query to handle.</param>
        /// <returns>An instance of an object as indicated in the query's return type.</returns>
        TResult Handle(TQuery query);
    }
}
