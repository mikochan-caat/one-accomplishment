﻿using Net40Utilities.Validation;
using OneAccomplishment.Core.Commands;
using OneAccomplishment.Core.Services;

namespace OneAccomplishment.Core.Decorators
{
    internal sealed class BusinessRulesValidatorCommandHandler<TCommand> : ICommandHandler<TCommand> 
        where TCommand : class, ICommand
    {
        private readonly ICommandHandler<TCommand> commandHandler;
        private readonly IBusinessRulesService rulesFactory;

        public BusinessRulesValidatorCommandHandler(
            ICommandHandler<TCommand> commandHandler, IBusinessRulesService rulesFactory)
        {
            ArgumentGuard.For(() => commandHandler).IsNull().Throw();
            ArgumentGuard.For(() => rulesFactory).IsNull().Throw();

            this.commandHandler = commandHandler;
            this.rulesFactory = rulesFactory;
        }

        public void Handle(TCommand command)
        {
            ArgumentGuard.For(() => command).IsNull().Throw();

            this.rulesFactory.GetRulesFor<TCommand>().ValidateCommand(command);
            this.commandHandler.Handle(command);
        }
    }
}
