﻿using System;
using Fasterflect;
using Net40Utilities.Validation;
using OneAccomplishment.Core.Services;
using OneAccomplishment.Core.Services.Auth;

namespace OneAccomplishment.Core.Decorators
{
    public abstract class AbstractPermissionsHandler
    {
        protected readonly IPrincipalService principalService;

        public AbstractPermissionsHandler(IPrincipalService principalService)
        {
            ArgumentGuard.For(() => principalService).IsNull().Throw();

            this.principalService = principalService;
        }

        protected void CheckPermissions(Type commandOrQueryType)
        {
            ArgumentGuard.For(() => commandOrQueryType).IsNull().Throw();

            var permissionFilter = commandOrQueryType.Attribute<PermissionFilterAttribute>();
            if (permissionFilter != null) {
                var userPermissions = this.principalService.CurrentPrincipal.Permissions;
                if (!permissionFilter.Demand(userPermissions)) {
                    string errorMessage = String.Format(
                        "Permissions check failed for '{0}'.", 
                        commandOrQueryType.FullName);
                    throw new UnauthorizedAccessException(errorMessage);
                }
            }
        }
    }
}
