﻿using System;

namespace OneAccomplishment.Core.Decorators.Config
{
    /// <summary>
    /// Marks an audited property as sensitive.
    /// This will redact the value of the property when logged.
    /// </summary>
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false, Inherited = false)]
    public sealed class AuditTrailSensitiveData : Attribute
    {
    }
}
