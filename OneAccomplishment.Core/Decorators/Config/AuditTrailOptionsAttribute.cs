﻿using System;

namespace OneAccomplishment.Core.Decorators.Config
{
    /// <summary>
    /// Controls the behavior for implementors of the IAuditable interface.
    /// </summary>
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false, Inherited = false)]
    public sealed class AuditTrailOptionsAttribute : Attribute
    {
        /// <summary>
        /// The message to use in the audit log.
        /// </summary>
        public string AuditMessage { get; private set; }

        /// <summary>Initializes a new instance of the <see cref="AuditTrailOptionsAttribute"/> class.</summary>
        /// <param name="auditMessage">The audit message.</param>
        public AuditTrailOptionsAttribute(string auditMessage)
        {
            this.AuditMessage = auditMessage;
        }
    }
}
