﻿using System;
using System.Linq;
using System.Reflection;
using Fasterflect;
using LinqToDB;
using Net40Utilities.Validation;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using OneAccomplishment.Core.Commands;
using OneAccomplishment.Core.Data;
using OneAccomplishment.Core.Decorators.Config;
using OneAccomplishment.Core.Decorators.Traits;
using OneAccomplishment.Core.Services;
using OneAccomplishment.Core.Services.Caching;

namespace OneAccomplishment.Core.Decorators
{
    internal sealed class AuditTrailCommandHandler<TCommand> : ICommandHandler<TCommand> 
        where TCommand : class, ICommand, IAuditable
    {
        private static readonly AuditTrailOptionsAttribute DefaultOptions =
            new AuditTrailOptionsAttribute("<No audit message defined>");
        private static readonly AuditTrailContractResolver AuditTrailJsonResolver =
            new AuditTrailContractResolver();

        private readonly ICommandHandler<TCommand> commandHandler;
        private readonly IClientService clientService;
        private readonly IPrincipalService principalService;
        private readonly UnitOfWork unitOfWork;

        public AuditTrailCommandHandler(
            ICommandHandler<TCommand> commandHandler, 
            IClientService clientService,
            IPrincipalService principalService, 
            UnitOfWork unitOfWork)
        {
            ArgumentGuard.For(() => commandHandler).IsNull().Throw();
            ArgumentGuard.For(() => clientService).IsNull().Throw();
            ArgumentGuard.For(() => principalService).IsNull().Throw();
            ArgumentGuard.For(() => unitOfWork).IsNull().Throw();
            
            this.commandHandler = commandHandler;
            this.clientService = clientService;
            this.principalService = principalService;
            this.unitOfWork = unitOfWork;
        }

        public void Handle(TCommand command)
        {
            ArgumentGuard.For(() => command).IsNull().Throw();

            this.commandHandler.Handle(command);

            var commandType = typeof(TCommand);
            var currentPrincipal = this.principalService.CurrentPrincipal;
            var requestInfo = this.clientService.CurrentRequest;
            var auditOptions = commandType.Attribute<AuditTrailOptionsAttribute>() ?? DefaultOptions;
            var traceData = JsonConvert.SerializeObject(command, new JsonSerializerSettings {
                ContractResolver = AuditTrailJsonResolver
            });

            var hashedUserAgent = this.unitOfWork.UserAgents
                .GetHashedUserAgent(this.unitOfWork, requestInfo.UserAgent);
            var userAgent = this.unitOfWork.UserAgents
                .Where(e => e.HashedValue == hashedUserAgent)
                .FromCache(this.unitOfWork, DbCacheScope.Global)
                .DefaultIfEmpty(new UserAgentEntity())
                .Single();

            this.unitOfWork.AuditTrails
                .Insert(() => new AuditTrailEntity {
                    AuditDateUtc = DateTime.UtcNow,
                    Username = currentPrincipal.Username,
                    IpAddress = requestInfo.ClientAddress,
                    UserAgentId = userAgent.UserAgentId,
                    AuditMessage = auditOptions.AuditMessage ?? String.Empty,
                    CommandType = commandType.Name,
                    Trace = traceData,
                    LoginRefId = currentPrincipal.LoginRefId
                });
        }

        private sealed class AuditTrailContractResolver : DefaultContractResolver
        {
            private static readonly SensitiveDataValueProvider SensitiveValueProvider =
                new SensitiveDataValueProvider();

            protected override JsonProperty CreateProperty(MemberInfo member, MemberSerialization memberSerialization)
            {
                var jsonProperty = base.CreateProperty(member, memberSerialization);

                var propertyInfo = member as PropertyInfo;
                if (propertyInfo != null && propertyInfo.HasAttribute<AuditTrailSensitiveData>()) {
                    jsonProperty.ValueProvider = SensitiveValueProvider;
                }

                return jsonProperty;
            }

            private sealed class SensitiveDataValueProvider : IValueProvider
            {
                public object GetValue(object target)
                {
                    return "********";
                }

                public void SetValue(object target, object value)
                {
                    throw new NotSupportedException();
                }
            }
        }
    }
}
