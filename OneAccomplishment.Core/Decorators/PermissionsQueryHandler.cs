﻿using Net40Utilities.Validation;
using OneAccomplishment.Core.Queries;
using OneAccomplishment.Core.Services;

namespace OneAccomplishment.Core.Decorators
{
    internal sealed class PermissionsQueryHandler<TQuery, TResult> 
        : AbstractPermissionsHandler, IQueryHandler<TQuery, TResult> 
        where TQuery : class, IQuery<TResult>
    {
        private readonly IQueryHandler<TQuery, TResult> queryHandler;

        public PermissionsQueryHandler(
            IQueryHandler<TQuery, TResult> queryHandler,
            IPrincipalService principalService)
            : base(principalService)
        {
            ArgumentGuard.For(() => queryHandler).IsNull().Throw();
            
            this.queryHandler = queryHandler;
        }

        public TResult Handle(TQuery command)
        {
            ArgumentGuard.For(() => command).IsNull().Throw();

            this.CheckPermissions(typeof(TQuery));
            return this.queryHandler.Handle(command);
        }
    }
}
