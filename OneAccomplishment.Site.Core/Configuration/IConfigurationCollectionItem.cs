﻿namespace OneAccomplishment.Site.Core.Configuration
{
    internal interface IConfigurationCollectionItem
    {
        object Key { get; }
    }
}
