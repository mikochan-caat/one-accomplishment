﻿using System;
using System.Configuration;
using Net40Utilities.Validation;

namespace OneAccomplishment.Site.Core.Configuration.Validators
{
    internal sealed class DoubleValidator : ConfigurationValidatorBase
    {
        public double MinValue { get; private set; }
        public double MaxValue { get; private set; }
        public bool IsExclusiveUpperBound { get; private set; }

        public DoubleValidator(double minValue, double maxValue)
            : this(minValue, maxValue, false)
        {
        }

        public DoubleValidator(double minValue, double maxValue, bool isExclusiveUpperBound)
        {
            ArgumentGuard.For(() => minValue).IsGreaterThan(maxValue).Throw();
            ArgumentGuard.For(() => maxValue).IsLessThan(minValue).Throw();

            this.MinValue = minValue;
            this.MaxValue = maxValue;
            this.IsExclusiveUpperBound = isExclusiveUpperBound;
        }

        public override bool CanValidate(Type type)
        {
            return type == typeof(double);
        }

        public override void Validate(object value)
        {
            double doubleValue;
            try {
                doubleValue = Convert.ToDouble(value);
            } catch (FormatException) {
                throw new ConfigurationErrorsException("Invalid double value encountered.");
            }

            double maxValue = this.MaxValue;
            if (this.IsExclusiveUpperBound) {
                maxValue--;
            }

            if (doubleValue < this.MinValue) {
                string errorMessage = String.Format(
                    "Double value {0} is less than the minimum allowed: {1}.",
                    doubleValue,
                    this.MinValue);
                throw new ConfigurationErrorsException(errorMessage);
            } else if (doubleValue > maxValue) {
                string errorMessage = String.Format(
                    "Double value {0} is greater than the maximum allowed: {1}.",
                    doubleValue,
                    this.MinValue);
                throw new ConfigurationErrorsException(errorMessage);
            }
        }
    }
}
