﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading;

namespace OneAccomplishment.Site.Core.Configuration
{
    internal sealed partial class HttpCompressionConfig : ConfigurationSection, IHttpCompressionConfig
    {
        private const string SupportedMethodsConfigName = "supportedMethods";

        private readonly Lazy<ISet<string>> supportedMethodsSet;

        ISet<string> IHttpCompressionConfig.SupportedMethods
        {
            get { return this.supportedMethodsSet.Value; }
        }

        [ConfigurationProperty(SupportedMethodsConfigName)]
        public SupportedMethodsElement SupportedMethods
        {
            get { return (SupportedMethodsElement)this[SupportedMethodsConfigName]; }
            set { this[SupportedMethodsConfigName] = value; }
        }

        public HttpCompressionConfig()
        {
            this.supportedMethodsSet = new Lazy<ISet<string>>(
                this.CreateSupportedMethodsSet,
                LazyThreadSafetyMode.ExecutionAndPublication);
        }

        private ISet<string> CreateSupportedMethodsSet()
        {
            return new HashSet<string>(
                this.SupportedMethods.Items.Select(item => item.Method),
                StringComparer.OrdinalIgnoreCase);
        }
    }
}
