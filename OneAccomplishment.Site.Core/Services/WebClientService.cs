﻿using System;
using System.Net;
using System.Threading;
using System.Web;
using Net40Utilities.Validation;
using OneAccomplishment.Core.Services;
using SimpleInjector;

namespace OneAccomplishment.Site.Core.Services
{
    public sealed class WebClientService : IClientService
    {
        private readonly Container container;
        private readonly Lazy<IRequestInfo> requestInfo;

        IRequestInfo IClientService.CurrentRequest
        {
            get { return this.requestInfo.Value; }
        }

        public WebClientService(Container container)
        {
            ArgumentGuard.For(() => container).IsNull().Throw();

            this.container = container;
            this.requestInfo = new Lazy<IRequestInfo>(
                this.GetRequestInfo,
                LazyThreadSafetyMode.ExecutionAndPublication);
        }

        private IRequestInfo GetRequestInfo()
        {
            var request = HttpContext.Current.Request;
            string hostAddress = request.UserHostAddress;
            IPAddress ipAddress;
            if (IPAddress.TryParse(hostAddress, out ipAddress) && IPAddress.IsLoopback(ipAddress)) {
                hostAddress = "127.0.0.1";
            }
            return new RequestInfo {
                ClientAddress = hostAddress,
                UserAgent = request.UserAgent
            };
        }

        private sealed class RequestInfo : IRequestInfo
        {
            public string ClientAddress { get; set; }
            public string UserAgent { get; set; }
        }
    }
}
