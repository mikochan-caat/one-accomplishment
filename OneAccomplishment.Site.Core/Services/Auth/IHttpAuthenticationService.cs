﻿using OneAccomplishment.Core.Services;

namespace OneAccomplishment.Site.Core.Services.Auth
{
    /// <summary>
    /// Provides an abstract API for HTTP-related authentication services.
    /// </summary>
    public interface IHttpAuthenticationService : ICoreService
    {
        /// <summary>
        /// Caches the current request's authentication cookie
        /// for the rest of this request.
        /// </summary>
        void CaptureAuthenticationCookie();
        /// <summary>
        /// Gets the landing URL after successful authentication.
        /// </summary>
        /// <returns>The URL to redirect to.</returns>
        string GetAuthenticationRedirectUrl();
    }
}
