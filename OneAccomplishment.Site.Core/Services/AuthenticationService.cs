﻿using System;
using System.Threading;
using System.Web;
using System.Web.Security;
using Net40Utilities.Validation;
using Newtonsoft.Json;
using OneAccomplishment.Core.Services;
using OneAccomplishment.Core.Services.Auth;
using OneAccomplishment.Site.Core.Services.Auth;
using SimpleInjector;

namespace OneAccomplishment.Site.Core.Services
{
    public sealed class AuthenticationService : 
        IAuthenticationService, IHttpAuthenticationService, IDisposable
    {
        private static readonly IAuthenticationTicket InvalidTicket = new InvalidAuthenticationTicket();

        private readonly Container container;
        private readonly IConfigurationService configService;
        private readonly Lazy<IExpiringAuthenticationTicket> currentTicket;

        private bool performedCapture;
        private string authenticationCookieValue;
        
        public event AuthenticationRevokedEventHandler OnAuthenticationRevoked;

        IExpiringAuthenticationTicket IAuthenticationService.CurrentTicket
        {
            get { return this.currentTicket.Value; }
        }

        public AuthenticationService(Container container, IConfigurationService configService)
        {
            ArgumentGuard.For(() => container).IsNull().Throw();
            ArgumentGuard.For(() => configService).IsNull().Throw();

            this.container = container;
            this.configService = configService;
            this.currentTicket = new Lazy<IExpiringAuthenticationTicket>(
                this.GetCurrentAuthenticationTicket,
                LazyThreadSafetyMode.ExecutionAndPublication);
            this.OnAuthenticationRevoked = delegate { };
        }

        void IAuthenticationService.GrantAuthentication(IAuthenticationTicket authTicket)
        {
            ArgumentGuard.For(() => authTicket).IsNull().Throw();
            if (!authTicket.IsValidTicket) {
                throw new InvalidOperationException("Cannot grant an invalid authentication ticket.");
            }

            double ticketDuration;
            bool persistTicket = false;
            if (authTicket.IsStayLoggedIn) {
                ticketDuration = this.configService.StayLoggedInMaxDuration;
                persistTicket = true;
            } else {
                ticketDuration = this.configService.StayLoggedInDefaultDuration;
            }

            string authJsonData = JsonConvert.SerializeObject(authTicket);
            var formsAuthTicket = new FormsAuthenticationTicket(
                1,
                authTicket.Username,
                DateTime.UtcNow,
                DateTime.UtcNow.AddHours(ticketDuration),
                persistTicket,
                authJsonData);
            string encryptedAuthTicket = FormsAuthentication.Encrypt(formsAuthTicket);
            var authCookie = new HttpCookie(FormsAuthentication.FormsCookieName, encryptedAuthTicket) {
                HttpOnly = true
            };
            if (persistTicket) {
                authCookie.Expires = formsAuthTicket.Expiration;
            }
            HttpContext.Current.Response.Cookies.Add(authCookie);
        }

        void IAuthenticationService.RevokeAuthentication(IAuthenticationTicket authTicket)
        {
            ArgumentGuard.For(() => authTicket).IsNull().Throw();

            if (!authTicket.IsValidTicket) {
                throw new InvalidOperationException("Cannot revoke an invalid authentication ticket.");
            }
            FormsAuthentication.SignOut();
            this.OnAuthenticationRevoked(authTicket);
        }

        void IHttpAuthenticationService.CaptureAuthenticationCookie()
        {
            if (!this.performedCapture) {
                this.performedCapture = true;
                var authCookie = HttpContext.Current.Request.Cookies[FormsAuthentication.FormsCookieName];
                if (authCookie != null) {
                    this.authenticationCookieValue = authCookie.Value;
                } else {
                    this.authenticationCookieValue = null;
                }
            } else {
                throw new InvalidOperationException(
                    "An attempt to capture the authentication cookie was already made.");
            }
        }

        string IHttpAuthenticationService.GetAuthenticationRedirectUrl()
        {
            return FormsAuthentication.GetRedirectUrl(String.Empty, false);
        }

        void IDisposable.Dispose()
        {
            foreach (var d in this.OnAuthenticationRevoked.GetInvocationList()) {
                this.OnAuthenticationRevoked -= (AuthenticationRevokedEventHandler)d;
            }
        }

        private IExpiringAuthenticationTicket GetCurrentAuthenticationTicket()
        {
            IAuthenticationTicket authTicket = null;
            FormsAuthenticationTicket formsAuthTicket = null;
            if (this.authenticationCookieValue != null) {
                formsAuthTicket = FormsAuthentication.Decrypt(this.authenticationCookieValue);
                authTicket = JsonConvert.DeserializeObject<AuthenticationTicket>(formsAuthTicket.UserData);
            } else {
                authTicket = InvalidTicket;
            }
            return new ExpiringAuthenticationTicket(authTicket, formsAuthTicket);
        }

        private sealed class ExpiringAuthenticationTicket : IExpiringAuthenticationTicket
        {
            private readonly IAuthenticationTicket authTicket;
            private readonly FormsAuthenticationTicket formsAuthTicket;

            public string Username { get { return this.authTicket.Username; } }
            public long LoginRefId { get { return this.authTicket.LoginRefId; } }
            public bool IsStayLoggedIn { get { return this.authTicket.IsStayLoggedIn; } }
            public bool IsValidTicket { get { return this.authTicket.IsValidTicket; } }

            public bool IsExpired
            {
                get
                {
                    if (this.formsAuthTicket == null) {
                        return true;
                    }
                    return this.formsAuthTicket.Expired;
                }
            }

            public ExpiringAuthenticationTicket(
                IAuthenticationTicket authTicket, FormsAuthenticationTicket formsAuthTicket)
            {
                this.authTicket = authTicket;
                this.formsAuthTicket = formsAuthTicket;
            }
        }

        private sealed class InvalidAuthenticationTicket : IAuthenticationTicket
        {
            string IAuthenticationTicket.Username { get { return "<Anonymous User>"; } }
            long IAuthenticationTicket.LoginRefId { get { return 0; } }
            bool IAuthenticationTicket.IsStayLoggedIn { get { return false; } }
            bool IAuthenticationTicket.IsValidTicket { get { return false; } }
        }
    }
}
