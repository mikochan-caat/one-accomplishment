﻿using System;
using System.Web.Mvc;
using Net40Utilities.Validation;
using OneAccomplishment.Core.BusinessRules;

namespace OneAccomplishment.Site.Core.Extensions
{
    public static class ExceptionsToModelStateExtensions
    {
        public static void AddBusinessRuleViolation<TViolation>(
            this ModelStateDictionary modelState, BusinessRuleViolationException<TViolation> ex)
            where TViolation : IRuleViolation<TViolation>
        {
            ArgumentGuard.For(() => modelState).IsNull().Throw();
            ArgumentGuard.For(() => ex).IsNull().Throw();

            string keyBase = String.Format("__bizRuleViolation_{0}", ex.GetHashCode());
            modelState.AddModelError(keyBase, ex.Message);
        }
    }
}
