﻿using System;
using System.Drawing;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Mvc;
using Net40Utilities.Validation;
using OneAccomplishment.Core.Services;
using OneAccomplishment.Core.Services.Caching;

namespace OneAccomplishment.Site.Core.Extensions
{
    public static class UserRandomColorExtensions
    {
        public static IHtmlString RenderUserRandomColor(
            this HtmlHelper helper, string username, string normalSelector, string hoveredSelector)
        {
            ArgumentGuard.For(() => helper).IsNull().Throw();
            ArgumentGuard.For(() => normalSelector).IsNull().Throw();
            ArgumentGuard.For(() => hoveredSelector).IsNull().Throw();

            var cacheService = DependencyResolver.Current.GetService<ICachingService>();
            var userColor = cacheService.GetOrAdd(
                username,
                () => {
                    using (var md5 = MD5.Create()) {
                        var hash = md5.ComputeHash(Encoding.UTF8.GetBytes(username));
                        string stringHash = BitConverter.ToString(hash).Replace("-", String.Empty);
                        string hex = stringHash.Substring(stringHash.Length - 6);

                        var randomColor = ColorTranslator.FromHtml("#" + hex);
                        return new UserRandomColor {
                            NormalColor = randomColor.Adjust(-0.2f),
                            HoveredColor = randomColor
                        };
                    }
                },
                new CacheOptions {
                    TypeKey = typeof(SharedLayoutExtensions)
                });

            var tagBuilder = new TagBuilder("style");
            tagBuilder.Attributes.Add("type", "text/css");
            tagBuilder.Attributes.Add("scoped", null);
            tagBuilder.InnerHtml = new StringBuilder(1024)
                .WriteBackgroundColorForSelector(normalSelector, userColor.NormalColor)
                .WriteBackgroundColorForSelector(hoveredSelector, userColor.HoveredColor)
                .ToString();

            return new MvcHtmlString(tagBuilder.ToString());
        }

        private static StringBuilder WriteBackgroundColorForSelector(
            this StringBuilder sb, string selector, Color color)
        {
            return sb.AppendFormat(
                "{0} {{ background-color: {1} !important; }} ", 
                selector, 
                ColorTranslator.ToHtml(color));
        }

        // Taken from: https://www.pvladov.com/2012/09/make-color-lighter-or-darker.html
        /// <summary>
        /// Creates a color with corrected brightness.
        /// </summary>
        /// <param name="color">Color to correct.</param>
        /// <param name="correctionFactor">The brightness correction factor. Must be between -1 and 1. 
        /// Negative values produce darker colors.</param>
        /// <returns>
        /// Corrected <see cref="Color"/> structure.
        /// </returns>
        private static Color Adjust(this Color color, float correctionFactor)
        {
            float red = (float)color.R;
            float green = (float)color.G;
            float blue = (float)color.B;

            if (correctionFactor < 0) {
                correctionFactor = 1 + correctionFactor;
                red *= correctionFactor;
                green *= correctionFactor;
                blue *= correctionFactor;
            } else {
                red = (255 - red) * correctionFactor + red;
                green = (255 - green) * correctionFactor + green;
                blue = (255 - blue) * correctionFactor + blue;
            }

            return Color.FromArgb(color.A, (int)red, (int)green, (int)blue);
        }

        private sealed class UserRandomColor
        {
            public Color NormalColor { get; set; }
            public Color HoveredColor { get; set; }
        }
    }
}
