﻿using System;
using System.Web;
using System.Web.Mvc;
using Net40Utilities.Validation;
using OneAccomplishment.Core.Services;

namespace OneAccomplishment.Site.Core.Extensions
{
    public static class TempDataCorrelationExtensions
    {
        public static readonly Type TypeItemKey = typeof(TempDataCorrelationExtensions);
        
        public static string GetTempDataCorrelationId(this ControllerBase controller)
        {
            ArgumentGuard.For(() => controller).IsNull().Throw();

            return GetTempDataCorrelationIdInternal(controller.ControllerContext.HttpContext);
        }

        public static string GetTempDataCorrelationId(this WebViewPage page)
        {
            ArgumentGuard.For(() => page).IsNull().Throw();

            return GetTempDataCorrelationIdInternal(page.Context);
        }

        /// <summary>
        /// Renders the TempData correlation ID in a HTML input tag.
        /// <para>
        /// This is useful for keeping the correlation IDs the same 
        /// between GET and POST requests.</para>
        /// </summary>
        /// <param name="helper">The HtmlHelper to use.</param>
        /// <returns>The TempData correlation ID rendered in a HTML input tag.</returns>
        public static IHtmlString TempDataCorrelationToken(this HtmlHelper helper)
        {
            ArgumentGuard.For(() => helper).IsNull().Throw();

            string correlationId = GetTempDataCorrelationIdInternal(helper.ViewContext.HttpContext);
            if (correlationId == null) {
                return null;
            }

            var configService = GetConfigurationService();
            var tagBuilder = new TagBuilder("input");
            tagBuilder.Attributes["type"] = "hidden";
            tagBuilder.Attributes["name"] = configService.TempDataCorrelationFormName;
            tagBuilder.Attributes["value"] = correlationId;

            return new MvcHtmlString(tagBuilder.ToString(TagRenderMode.SelfClosing));
        }

        private static string GetTempDataCorrelationIdInternal(HttpContextBase context)
        {
            var httpItems = context.Items;
            string correlationId = httpItems[TypeItemKey] as string;
            if (String.IsNullOrWhiteSpace(correlationId)) {
                correlationId = null;
            }
            return correlationId;
        }

        private static IConfigurationService GetConfigurationService()
        {
            return DependencyResolver.Current.GetService<IConfigurationService>();
        }
    }
}
