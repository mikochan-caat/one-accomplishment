﻿using System;
using System.Web;
using System.Web.Mvc;
using Net40Utilities.Validation;

namespace OneAccomplishment.Site.Core.Extensions
{
    public static class SharedLayoutExtensions
    {
        public static IHtmlString RenderAssemblyVersion(this WebViewPage page, Type type)
        {
            ArgumentGuard.For(() => page).IsNull().Throw();
            ArgumentGuard.For(() => type).IsNull().Throw();

            return new MvcHtmlString(String.Format("v{0}", type.Assembly.GetName().Version));
        }

        public static IHtmlString RenderCopyright(this WebViewPage page)
        {
            ArgumentGuard.For(() => page).IsNull().Throw();

            const int baselineYear = 2018;
            int currentYear = DateTime.UtcNow.Year;
            string copyrightString;
            if (currentYear > baselineYear) {
                copyrightString = String.Format("{0}-{1}", baselineYear, currentYear);
            } else {
                copyrightString = baselineYear.ToString();
            }

            return new MvcHtmlString(copyrightString);
        }
    }
}
