﻿using System.Linq;
using Fasterflect;
using Net40Utilities.Validation;
using OneAccomplishment.Core.Services.Auth;

namespace OneAccomplishment.Site.Core.Mvc
{
    public static class ModelPermissionFilters
    {
        public static T FilterModelBooleans<T>(T model, PermissionSet permissions) where T : class
        {
            ArgumentGuard.For(() => model).IsNull().Throw();
            ArgumentGuard.For(() => permissions).IsNull().Throw();

            var properties = typeof(T)
                .PropertiesWith(Flags.InstancePublic, typeof(PermissionFilterAttribute))
                .Where(prop => prop.PropertyType == typeof(bool));
            foreach (var prop in properties) {
                var permissionFilter = prop.Attribute<PermissionFilterAttribute>();
                prop.Set(model, permissionFilter.Demand(permissions));
            }
            return model;
        }
    }
}
