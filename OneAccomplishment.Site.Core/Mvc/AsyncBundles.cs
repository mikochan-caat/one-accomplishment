﻿using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;

namespace OneAccomplishment.Site.Core.Mvc
{
    public static class AsyncBundles
    {
        public static IHtmlString RenderStyleNoscript(string virtualPath)
        {
            var tagBuilder = new TagBuilder("link");
            tagBuilder.Attributes.Add("rel", "stylesheet");
            tagBuilder.Attributes.Add("href", "{0}");

            return Styles.RenderFormat(
                "<noscript>" + tagBuilder.ToString(TagRenderMode.SelfClosing) + "</noscript>",
                virtualPath);
        }

        public static IHtmlString RenderStyleAsync(string virtualPath)
        {
            var tagBuilder = new TagBuilder("link");
            tagBuilder.Attributes.Add("rel", "stylesheet");
            tagBuilder.Attributes.Add("href", "{0}");
            tagBuilder.Attributes.Add("media", "none");
            tagBuilder.Attributes.Add(
                "onload", 
                "this.media = 'all'; this.attributes.removeNamedItem('onload');");

            return Styles.RenderFormat(tagBuilder.ToString(TagRenderMode.SelfClosing), virtualPath);
        }

        public static IHtmlString RenderScriptAsync(string virtualPath)
        {
            var tagBuilder = new TagBuilder("script");
            tagBuilder.Attributes.Add("type", "text/javascript");
            tagBuilder.Attributes.Add("src", "{0}");
            string loadType;
            if (BundleTable.EnableOptimizations) {
                loadType = "async";
            } else {
                loadType = "defer";
            }
            tagBuilder.Attributes.Add(loadType, null);

            return Scripts.RenderFormat(tagBuilder.ToString(), virtualPath);
        }
    }
}
