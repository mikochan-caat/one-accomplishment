﻿// Taken from http://kozmic.net/2011/01/05/lock-free-thread-safe-initialisation-in-c/
// No license provided
using System;
using System.Threading;

namespace OneAccomplishment.Site.Core.Utilities
{
    /// <summary>
    /// Provides thread-safe lock-free one time initialization blocks
    /// for resources.
    /// </summary>
    public sealed class ThreadSafeInit
    {
        // We are free to use negative values here, as ManagedThreadId is guaranteed to be always > 0
        // http://www.netframeworkdev.com/net-base-class-library/threadmanagedthreadid-18626.shtml
        // the ids can be recycled as mentioned, but that does not affect us since at any given point in time
        // there can be no two threads with the same managed id, and that's all we care about
        private const int Initialized = Int32.MinValue + 1;
        private const int NotInitialized = Int32.MinValue;

        private int state = NotInitialized;

        /// <summary>
        /// Ends the thread-safe initialization block and marks this
        /// object as initialized.
        /// </summary>
        public void EndThreadSafeOnceSection()
        {
            if (this.state == Initialized) {
                return;
            }
            if (this.state == Thread.CurrentThread.ManagedThreadId) {
                this.state = Initialized;
            }
        }

        /// <summary>
        /// Begins a thread-safe initialization block.
        /// </summary>
        /// <returns>true if the current thread is the one that performed the one time
        /// initialization, otherwise false.</returns>
        public bool ExecuteThreadSafeOnce()
        {
            if (this.state == Initialized) {
                return false;
            }

            var inProgressByThisThread = Thread.CurrentThread.ManagedThreadId;
            var preexistingState = Interlocked.CompareExchange(ref this.state, inProgressByThisThread, NotInitialized);
            if (preexistingState == NotInitialized) {
                return true;
            }
            if (preexistingState == Initialized || preexistingState == inProgressByThisThread) {
                return false;
            }

            while (this.state != Initialized) {
                Thread.SpinWait(1);
            }
            return false;
        }
    }
}
