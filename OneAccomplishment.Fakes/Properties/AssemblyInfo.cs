﻿using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("One Accomplishment .NET Fakes Library")]
[assembly: AssemblyDescription("TMS task accomplishments generator")]
[assembly: AssemblyProduct("OneAccomplishment")]
[assembly: AssemblyCopyright("Copyright © 2019 ITDCAT")]

[assembly: ComVisible(false)]
[assembly: Guid("cfac3944-fded-4190-ae55-b4316e640fa8")]

[assembly: AssemblyVersion("1.0.*")]
